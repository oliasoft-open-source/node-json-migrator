import { promises as fs } from 'fs';
import path from 'path';
import mockFs from 'mock-fs';
import { migrate } from './migrate';
import { entity, getTestDatabase, pgpHelpers } from '../../test/test-database';

const directory = 'path/to/migrations';

const consoleWarnSpy = jest.spyOn(console, 'warn');
const consoleErrorSpy = jest.spyOn(console, 'error');

const fsMockModules = {
  node_modules: mockFs.load(path.resolve(__dirname, '../../node_modules')),
};

describe('Behavioural tests (without DB)', () => {
  afterEach(() => {
    mockFs.restore();
    jest.clearAllMocks();
  });
  it('executes all migrations without database when all option is set', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
      },
    });
    const payload = {};
    const config = {
      directory,
    };

    const migratedPayload = await migrate({
      payload,
      config,
    });
    expect(migratedPayload.nextPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
    });
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
  });
  it('dry option does not write to files or database', async () => {
    const database = await getTestDatabase();
    const config = {
      directory,
      database,
      entity,
      pgpHelpers,
    };
    const writeFileSpy = jest.spyOn(fs, 'writeFile');
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
      },
    });
    const payload = {};
    const version = undefined;

    const migratedPayload = await migrate({
      payload,
      config: {
        ...config,
        version,
        dry: true,
      },
    });
    const migrationHistoryCount = (
      await database.one('SELECT COUNT(*) FROM animals_migrations')
    ).count;
    const versionCount = (
      await database.one('SELECT COUNT(*) FROM animals_versions')
    ).count;
    expect(migratedPayload.nextPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
    });
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    expect(migrationHistoryCount).toBe(0); //no writing of file history to DB
    expect(versionCount).toBe(0); //no writing of version history to DB
    expect(writeFileSpy).not.toHaveBeenCalled(); //no overwriting of plan.json
  });
});
