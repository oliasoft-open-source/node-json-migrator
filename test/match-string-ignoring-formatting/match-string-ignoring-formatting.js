import { isEqual } from 'lodash';
import { matcherHint, printReceived, printExpected } from 'jest-matcher-utils';
import { diff } from 'jest-diff';

const replaceLineBreaksAndWhitespace = (input) =>
  input.replaceAll(/\r?\n|\r/g, '').replace(/\s+/g, '');

const name = `toEqualWithCompressedWhitespace`;

/**
 * Custom Jest matcher for comparing strings, ignoring line breaks and whitespace
 * based on https://stackoverflow.com/a/48459005/942635
 */

// eslint-disable-next-line func-names
export default function (received, expected) {
  const receivedWithCompressedWhitespace =
    replaceLineBreaksAndWhitespace(received);
  const expectedWithCompressedWhitespace =
    replaceLineBreaksAndWhitespace(expected);
  const pass = isEqual(
    receivedWithCompressedWhitespace,
    expectedWithCompressedWhitespace,
  );
  const message = pass
    ? () =>
        `${matcherHint(`.not.${name}`)}\n\n` +
        `Uncompressed expected value:\n` +
        `  ${printExpected(expected)}\n` +
        `Expected value with compressed whitespace to not equal:\n` +
        `  ${printExpected(expectedWithCompressedWhitespace)}\n` +
        `Uncompressed received value:\n` +
        `  ${printReceived(received)}\n` +
        `Received value with compressed whitespace:\n` +
        `  ${printReceived(receivedWithCompressedWhitespace)}`
    : () => {
        const diffString = diff(
          expectedWithCompressedWhitespace,
          receivedWithCompressedWhitespace,
          {
            expand: this.expand,
          },
        );
        return (
          `${matcherHint(`.${name}`)}\n\n` +
          `Uncompressed expected value:\n` +
          `  ${printExpected(expected)}\n` +
          `Expected value with compressed whitespace to equal:\n` +
          `  ${printExpected(expectedWithCompressedWhitespace)}\n` +
          `Uncompressed received value:\n` +
          `  ${printReceived(received)}\n` +
          `Received value with compressed whitespace:\n` +
          `  ${printReceived(receivedWithCompressedWhitespace)}${
            diffString ? `\n\nDifference:\n\n${diffString}` : ``
          }`
        );
      };
  return {
    actual: received,
    expected,
    message,
    name,
    pass,
  };
}
