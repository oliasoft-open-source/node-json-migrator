import path from 'path';
import { merge } from 'lodash';
import { promises as fs } from 'fs';
import { importFromString } from 'module-from-string';
import { hash } from '../hash/hash';
import { getMigrationFilePaths, getSkippedFilePaths } from '../glob/glob';
import {
  throwIfFileNamesInvalid,
  throwIfFilesNotFound,
  warnIfFilesHaveBeenRemovedFromPlan,
  throwIfFilesHaveChanged,
  throwIfSequenceHasChanged,
  throwIfSequenceNotUnique,
  validateFileName,
  validatePlan,
  throwIfFileNamesNotUnique,
  throwIfSequenceHasIntegerGaps,
} from './validator';
import {
  getMigrationRecords,
  replaceMigrationsRecords,
  insertVersions,
  createMigrationsTables,
  migrationTablesExist,
} from '../database/database';
import {
  setCachedPlannedVersion,
  getCachedPlannedVersion,
  setCachedMigratorFunctions,
  getCachedMigratorFunctions,
  cacheHasAllMigrationFunctions,
} from './cached-planned-version';

/**
 * Format JSON with 2-space indentation
 *
 * @param {String} json
 * @returns {String} formatted json
 */
export const formatJSON = (json) => {
  try {
    return JSON.stringify(JSON.parse(json), null, 2);
  } catch (e) {
    console.warn('Unable to parse JSON');
    throw new Error(e);
  }
};

//https://stackoverflow.com/a/31102605/942635
const sortEntry = (entry) =>
  Object.keys(entry)
    .sort()
    .reduce((obj, key) => {
      obj[key] = entry[key];
      return obj;
    }, {});

/**
 * Sorts the plan entries by sequence string
 *
 * @param {Array<Object>} plannedMigrations
 * returns Array<Object>
 */

const maxSequence = 1000000;

export const sortPlanEntries = (plannedMigrations) =>
  plannedMigrations
    //dotted sequence sort https://stackoverflow.com/a/40201629/942635
    .sort((a, b) =>
      a.sequence
        .replace(/\d+/g, (n) => +n + maxSequence)
        .localeCompare(b.sequence.replace(/\d+/g, (n) => +n + maxSequence)),
    )
    .map((p) => sortEntry(p));

/**
 * Get next automatic sequence string
 *
 * @param {String} sortedPlannedMigrations
 * returns String next sequence
 */

export const getNextSequenceString = (sortedPlannedMigrations) => {
  const lastMigration =
    sortedPlannedMigrations[sortedPlannedMigrations.length - 1];
  if (!lastMigration) {
    return '1';
  }
  const currentLastSequenceNumber = lastMigration.sequence;
  return `${parseInt(`${currentLastSequenceNumber}`, 10) + 1}`;
};

/**
 * Read the plan.json file
 *
 * @param {String} directory path to migrations directory
 * returns Promise<String> plan.json string
 */

export const readPlan = async (directory) => {
  let plan;
  try {
    plan = await fs.readFile(`${directory}/plan.json`, {
      encoding: 'utf8',
    });
    return formatJSON(plan);
  } catch {
    throw new Error('Invalid JSON for migrator plan.js (unable to parse file)');
  }
};

/**
 * Parse the plan.json string into a sorted object
 *
 * @param {String} plan plan.json string
 * returns Array<Object> migration entries from plan.json
 */

export const parsePlan = (plan) => {
  let plannedMigrations;
  try {
    plannedMigrations = JSON.parse(plan);
  } catch {
    throw new Error('Invalid JSON for migrator plan.js (unable to parse file)');
  }
  if (!validatePlan(plannedMigrations)) {
    throw new Error(
      'Invalid JSON for migrator plan.js (does not match schema)',
    );
  }
  return sortPlanEntries(plannedMigrations);
};

/**
 * Write the plan.json file
 *
 * @param {String} directory path to migrations directory
 * @param {String} content string to be written to file
 * returns Promise<>
 */

export const writePlan = async (directory, content) => {
  //todo this is not being written in the correct order
  await fs.writeFile(path.resolve(`${directory}/plan.json`), content, {
    encoding: 'utf8',
  });
};

/**
 * Generate plan entries
 *
 * @param {Array<Object>} sortedMigrationEntries
 * returns Array<Object>
 */

export const generatePlanEntries = (sortedMigrationEntries) =>
  sortedMigrationEntries.map((m) => ({
    fileHash: m.fileHash,
    fileName: m.fileName,
    sequence: m.sequence,
  }));

export const loadMigrationFunctionsFromStrings = async (
  sortedMigrationEntries,
  importModule,
) =>
  Promise.all(
    sortedMigrationEntries.map(async (m) => {
      const { script } = m;
      const migrator =
        script && importModule
          ? (await importFromString(script))?.default
          : null;
      return {
        fileHash: m.fileHash,
        migrator,
      };
    }),
  );

export const loadMigrationFunctions = async (
  sortedMigrationEntries,
  nextVersion,
  importModule = true,
) => {
  const cachedPlannedVersion = getCachedPlannedVersion();
  const cachedMigratorFunctions = getCachedMigratorFunctions();
  const cacheValid =
    cachedPlannedVersion === nextVersion &&
    cacheHasAllMigrationFunctions({
      sortedMigrationEntries,
      cachedMigrationEntries: cachedMigratorFunctions,
    });
  /*
    OW-15584 the module-from-string package seems to have a memory leak bug
    where calling importFromString multiple times consumes memory that
    doesn't get released or garbage collected. We cache the imported strings
    as a workaround (until plan.json version changes)
   */
  const migrationFunctions = cacheValid
    ? cachedMigratorFunctions
    : await loadMigrationFunctionsFromStrings(
        sortedMigrationEntries,
        importModule,
      );
  if (!cacheValid) {
    setCachedMigratorFunctions(migrationFunctions);
    setCachedPlannedVersion(nextVersion);
  }
  return merge(sortedMigrationEntries, migrationFunctions);
};

export const getPlannedMigrations = async ({ config }) => {
  const {
    directory,
    database,
    entity,
    force,
    pgpHelpers,
    dry,
    importModule = true,
  } = config;

  if (database && !dry) {
    /*
      OW-9043 and OW-9238: The tool tries to self-create its own meta tables
      to simplify setup. That's useful for devs running on local machines,
      and when running via CLI jobs. But for environments without CREATE
      privileges, we want this don't want to execute CREATE TABLE IF NOT EXISTS
      because it throws errors.
    */
    const tablesExist = await migrationTablesExist(database, entity);
    if (!tablesExist) {
      await createMigrationsTables(database, entity);
    }
  }

  const rawPlan = await readPlan(directory);
  const parsedPlan = parsePlan(rawPlan);

  const plannedMigrations = parsedPlan.map((m) => ({
    fileHashFromPlan: m.fileHash,
    fileName: m.fileName,
    sequence: m.sequence,
  }));

  const filePaths = await getMigrationFilePaths(directory);

  const skippedFilePaths = await getSkippedFilePaths(directory);

  const plannedMigrationsWithFileEntries = await Promise.all(
    plannedMigrations.map(async (m) => {
      const { fileName } = m;
      const isValidFileName = validateFileName(fileName);
      const filePath = filePaths.find((f) => path.parse(f).base === m.fileName);
      const script = filePath
        ? await fs.readFile(filePath, { encoding: 'utf8' })
        : null;
      const fileHash = script ? hash(script) : null;

      //skipped files
      const skipMatch = '(.skip|.skip\\d+).js$';
      const nameMatch = path.parse(m.fileName).name;
      const regex = new RegExp(`${nameMatch}${skipMatch}`, 'g');
      const matchingSkippedFilePaths = skippedFilePaths.filter((f) => {
        const name = path.parse(f).base;
        return name.match(regex);
      });
      const skippedFileHashes = await Promise.all(
        matchingSkippedFilePaths.map(async (skippedFilePath) => {
          const skippedScript = await fs.readFile(skippedFilePath, {
            encoding: 'utf8',
          });
          return skippedScript ? hash(skippedScript) : null;
        }),
      );
      return {
        ...m,
        filePath,
        isValidFileName,
        script,
        fileHash,
        skippedFileHashes,
      };
    }),
  );

  const historicalMigrations = database
    ? await getMigrationRecords(database, entity)
    : [];

  const plannedMigrationsWithHistory = plannedMigrationsWithFileEntries.map(
    (m) => {
      const historicalMigration = historicalMigrations.find(
        (h) => h.fileName === m.fileName,
      );
      return {
        ...m,
        fileHashFromHistory: historicalMigration?.fileHash || null,
        sequenceFromHistory: historicalMigration?.sequence || null,
      };
    },
  );

  throwIfFilesNotFound(plannedMigrationsWithHistory, importModule);
  throwIfFileNamesInvalid(plannedMigrationsWithHistory);
  throwIfFileNamesNotUnique(plannedMigrationsWithHistory);
  throwIfSequenceNotUnique(plannedMigrationsWithHistory);
  if (!force) {
    warnIfFilesHaveBeenRemovedFromPlan(
      plannedMigrationsWithHistory,
      historicalMigrations,
    );
    throwIfFilesHaveChanged(plannedMigrationsWithHistory);
    throwIfSequenceHasChanged(plannedMigrationsWithHistory);
  }
  throwIfSequenceHasIntegerGaps(plannedMigrationsWithHistory);
  const validatedPlannedMigrations = generatePlanEntries(
    plannedMigrationsWithHistory,
  );
  const validatedPlan = JSON.stringify(validatedPlannedMigrations, null, 2);
  const nextVersion = hash(validatedPlan);
  const planHasChanged = validatedPlan !== rawPlan;

  if (!dry && planHasChanged) {
    await writePlan(directory, validatedPlan);
  }

  if (database && !dry) {
    await replaceMigrationsRecords(
      database,
      pgpHelpers,
      entity,
      validatedPlannedMigrations,
    );
    const versionRecord = [{ version: nextVersion, plan: validatedPlan }];
    await insertVersions(database, pgpHelpers, entity, versionRecord);
  }

  const plannedMigrationsWithFunctions = await loadMigrationFunctions(
    plannedMigrationsWithHistory,
    nextVersion,
    importModule,
  );
  return {
    plannedMigrations: plannedMigrationsWithFunctions,
    nextVersion,
  };
};
