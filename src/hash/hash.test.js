import { hash } from './hash';

const validSHA = /^[a-f0-9]{64}$/gi;

describe('Helpers', () => {
  describe('hash', () => {
    it('generates a valid SHA256 hash', async () => {
      const sha = hash('monkeys like bananas');
      expect(validSHA.test(sha)).toBe(true);
    });
  });
});
