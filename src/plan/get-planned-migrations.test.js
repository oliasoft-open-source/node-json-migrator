import path from 'path';
import mockFs from 'mock-fs';
import { promises as fs } from 'fs';
import { getPlannedMigrations } from './plan';
import { pgpHelpers, entity, getTestDatabase } from '../../test/test-database';

const fsMockModules = {
  node_modules: mockFs.load(path.resolve(__dirname, '../../node_modules')),
};

jest.mock('../database/database', () => ({
  __esModule: true,
  ...jest.requireActual('../database/database'),
  migrationTablesExist: jest.fn().mockImplementation(() => {
    return true;
  }),
}));

const directory = 'path/to/migrations';
let database;
let config;

const consoleErrorSpy = jest.spyOn(console, 'error');
const consoleWarnSpy = jest.spyOn(console, 'warn');

//for tests that rely on exact formatting / indentation
const formattedPlan = `[
  {
    "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
    "fileName": "add-panda.js",
    "sequence": "1"
  },
  {
    "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
    "fileName": "add-zebra.js",
    "sequence": "2"
  }
]`;

describe('Plan', () => {
  describe('getPlannedMigrations', () => {
    beforeEach(async () => {
      jest.clearAllMocks();
      database = await getTestDatabase();
      config = {
        directory,
        database,
        entity,
        pgpHelpers,
      };
    });
    afterEach(() => {
      mockFs.restore();
    });
    it('gets the planned migrations', async () => {
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
          'add-panda': {
            'add-panda.js':
              'export default (payload) => ({...payload, panda: "Ralph"});',
          },
          'add-zebra': {
            'add-zebra.js':
              'export default (payload) => ({...payload, zebra: "Lachlan"});',
          },
        },
      });
      const { plannedMigrations } = await getPlannedMigrations({ config });
      expect(plannedMigrations.length).toBe(2);
      expect(plannedMigrations[0].fileName).toBe('add-panda.js');
      expect(plannedMigrations[1].fileName).toBe('add-zebra.js');
      expect(consoleErrorSpy).not.toHaveBeenCalled();
    });
    it('throws error when file(s) are missing', async () => {
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
          'add-zebra': {
            'add-zebra.js':
              'export default (payload) => ({...payload, zebra: "Lachlan"});',
          },
        },
      });
      await expect(getPlannedMigrations({ config })).rejects.toThrow(
        'Migration files from plan.json are missing from filesystem',
      );
      expect(consoleErrorSpy).toHaveBeenCalledTimes(1);
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringContaining('add-panda.js'),
      );
    });
    it('warns when migration(s) have been deleted from plan', async () => {
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': `[
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "1.1"
            }
          ]`,
          'add-panda': {
            'add-panda.js':
              'export default (payload) => ({...payload, panda: "Ralph"});',
          },
          'add-zebra': {
            'add-zebra.js':
              'export default (payload) => ({...payload, zebra: "Lachlan"});',
          },
        },
      });
      await database.any(`
        INSERT INTO animals_migrations (file_hash, file_name, sequence)
        VALUES (
          '4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c',
          'add-panda.js',
          '1'
        )
      `);
      await getPlannedMigrations({ config });
      expect(consoleWarnSpy).toHaveBeenCalledTimes(2);
      expect(consoleWarnSpy).toHaveBeenCalledWith(
        expect.stringContaining('add-panda.js'),
      );
      expect(consoleWarnSpy).toHaveBeenCalledWith(
        expect.stringContaining(
          'Previously executed migration files have been deleted from plan.json (rename file to .skip.js and replace with a new file instead)',
        ),
      );
    });
    it('does not warn when migration(s) have been deleted from plan and force is enabled', async () => {
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': `[
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "1.1"
            }
          ]`,
          'add-panda': {
            'add-panda.js':
              'export default (payload) => ({...payload, panda: "Ralph"});',
          },
          'add-zebra': {
            'add-zebra.js':
              'export default (payload) => ({...payload, zebra: "Lachlan"});',
          },
        },
      });
      await database.any(`
        INSERT INTO animals_migrations (file_hash, file_name, sequence)
        VALUES (
          '4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c',
          'add-panda.js',
          '1'
        )
      `);
      await getPlannedMigrations({ config: { ...config, force: true } });
      expect(consoleWarnSpy).not.toHaveBeenCalled();
    });
    it('throws when migration files(s) have been altered', async () => {
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': `[
            {
              "fileHash": "6f171b76e6c4f358e07d0473dfa37b5cb2c721e90c8f716863f8b9d3d2bd97da",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
          'add-panda': {
            'add-panda.js':
              'export default (payload) => ({...payload, panda: "Ralph"});',
          },
          'add-zebra': {
            'add-zebra.js':
              'export default (payload) => ({...payload, zebra: "Lachlan"});',
          },
        },
      });
      await database.any(`
        INSERT INTO animals_migrations (file_hash, file_name, sequence)
        VALUES (
          '6f171b76e6c4f358e07d0473dfa37b5cb2c721e90c8f716863f8b9d3d2bd97da',
          'add-panda.js',
          '1'
        ), (
          '47a4acb7b44c047684a68e05806e6ebf4ee36c676b30d8328fb2682baac780c0',
          'add-zebra.js',
          '2'
        )
      `);
      await expect(getPlannedMigrations({ config })).rejects.toThrow(
        'Not allowed to change migration files (for unreleased local work, you can use the `force` option)',
      );
      expect(consoleErrorSpy).toHaveBeenCalledTimes(2);
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringContaining('add-panda.js'),
      );
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringContaining('add-zebra.js'),
      );
    });
    it('does not throw when migration files(s) have been altered and force is enabled', async () => {
      mockFs({
        ...fsMockModules,
        [directory]: {
          //incorrect hash for panda, correct has for zebra
          'plan.json': `[
            {
              "fileHash": "6f171b76e6c4f358e07d0473dfa37b5cb2c721e90c8f716863f8b9d3d2bd97da",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
          'add-panda': {
            'add-panda.js':
              'export default (payload) => ({...payload, panda: "Ralph"});', //4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c
          },
          'add-zebra': {
            'add-zebra.js':
              'export default (payload) => ({...payload, zebra: "Lachlan"});', //bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e
          },
        },
      });
      await database.any(`
        INSERT INTO animals_migrations (file_hash, file_name, sequence)
        VALUES (
          '4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c', --correct hash
          'add-panda.js',
          '1'
        ), (
          'd8216a6d4b452435422cd8367278c24e31de61574b0f6a15e84ebdae1322431e', --incorrect hash
          'add-zebra.js',
          '2'
        )
      `);
      await getPlannedMigrations({ config: { ...config, force: true } });
      const updatedPlan = await fs.readFile(`${directory}/plan.json`, {
        encoding: 'utf8',
      });
      expect(updatedPlan).toMatchStringIgnoringFormatting(`[
        {
          "fileHash":
            "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
          "fileName": "add-panda.js",
          "sequence": "1"
        },
        {
          "fileHash":
            "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
          "fileName": "add-zebra.js",
          "sequence": "2"
        }
      ]`);
      expect(consoleErrorSpy).not.toHaveBeenCalled();
    });
    it('throws when migration sequences(s) have been altered', async () => {
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "3"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
          'add-panda': {
            'add-panda.js':
              'export default (payload) => ({...payload, panda: "Ralph"});',
          },
          'add-zebra': {
            'add-zebra.js':
              'export default (payload) => ({...payload, zebra: "Lachlan"});',
          },
        },
      });
      await database.any(`
        INSERT INTO animals_migrations (file_hash, file_name, sequence)
        VALUES (
          '4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c',
          'add-panda.js',
          '1'
        ), (
          'bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e',
          'add-zebra.js',
          '2'
        )
      `);
      await expect(getPlannedMigrations({ config })).rejects.toThrow(
        'Not allowed to change migration sequences in plan.json',
      );
      expect(consoleErrorSpy).toHaveBeenCalledTimes(1);
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringContaining('add-panda.js'),
      );
    });
    it('does not throw when migration sequences(s) have been altered and force is enabled', async () => {
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1.1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
          'add-panda': {
            'add-panda.js':
              'export default (payload) => ({...payload, panda: "Ralph"});',
          },
          'add-zebra': {
            'add-zebra.js':
              'export default (payload) => ({...payload, zebra: "Lachlan"});',
          },
        },
      });
      await database.any(`
        INSERT INTO animals_migrations (file_hash, file_name, sequence)
        VALUES (
          '4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c',
          'add-panda.js',
          '1'
        ), (
          'bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e',
          'add-zebra.js',
          '2'
        )
      `);
      await getPlannedMigrations({ config: { ...config, force: true } });
      expect(consoleErrorSpy).not.toHaveBeenCalled();
    });
    it('throws when there are duplicated sequence numbers', async () => {
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "1"
            }
          ]`,
          'add-panda': {
            'add-panda.js':
              'export default (payload) => ({...payload, panda: "Ralph"});',
          },
          'add-zebra': {
            'add-zebra.js':
              'export default (payload) => ({...payload, zebra: "Lachlan"});',
          },
        },
      });
      await expect(getPlannedMigrations({ config })).rejects.toThrow(
        'Migrations must have unique sequence numbers in plan.json',
      );
      expect(consoleErrorSpy).toHaveBeenCalledTimes(2);
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringContaining('add-panda.js'),
      );
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringContaining('add-zebra.js'),
      );
    });
    it('does not rewrite plan.json when it is already correct', async () => {
      const writeFileSpy = jest.spyOn(fs, 'writeFile');
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': formattedPlan,
          'add-panda': {
            'add-panda.js':
              'export default (payload) => ({...payload, panda: "Ralph"});',
          },
          'add-zebra': {
            'add-zebra.js':
              'export default (payload) => ({...payload, zebra: "Lachlan"});',
          },
        },
      });
      await getPlannedMigrations({ config });
      expect(writeFileSpy).not.toHaveBeenCalled();
    });
    it('rewrites plan.json when it is not already correct', async () => {
      const writeFileSpy = jest.spyOn(fs, 'writeFile');
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': formattedPlan,
          'add-panda': {
            'add-panda.js':
              'export default (payload) => ({...payload, panda: "Ralph modified"});',
          },
          'add-zebra': {
            'add-zebra.js':
              'export default (payload) => ({...payload, zebra: "Lachlan"});',
          },
        },
      });
      await getPlannedMigrations({
        config: {
          ...config,
          force: true,
        },
      });
      expect(writeFileSpy).toHaveBeenCalledTimes(1);
    });
  });
});
