import chalk from 'chalk';
import process from 'process';
import { execSync, spawn } from 'child_process';
import { hash } from '../hash/hash';

const { platform } = process;
export const isWindows = platform === 'win32';

export const terminals = Object.freeze({
  bash: 'bash',
  cmd: 'cmd',
});

export const historyError = `Unable to fetch git history. Possible reasons:
    - Have you merged latest master branch into your environment (pull latest migrations)?
    - Are you trying to downgrade data (not allowed), e.g. newer data into an older environment?
        - Not allowed to export from TEST and import to PROD
        - Not allowed to export datasets generated from migration feature branches into TEST, PROD or master ENVs
    - On Windows? Do you have git and a terminal (bash or cmd) in your PATH? Try cmder.app, gitforwindows.org, or WSL
`;

export const commandAvailable = (command) => {
  try {
    const exists = isWindows ? 'where' : 'which';
    execSync(`${exists} ${command}`);
    return true;
  } catch (error) {
    return false;
  }
};

export const gitAvailable = () => commandAvailable('git');

export const cmdAvailable = () => isWindows && commandAvailable('cmd');

export const bashAvailable = () => {
  if (commandAvailable('bash')) {
    try {
      const response = execSync('bash --version').toString();
      return response.includes('version');
    } catch (error) {
      return false;
    }
  }
  return false;
};

export const getTerminal = () => {
  const bashActive = bashAvailable();
  const cmdActive = cmdAvailable();
  if (!(bashActive || cmdActive)) {
    throw new Error(
      'bash terminal (or cmd.exe on Windows) must be installed and in PATH',
    );
  }
  return isWindows && !bashActive ? terminals.cmd : terminals.bash;
};

export const exec = (command, terminal) =>
  new Promise((resolve, reject) => {
    const useWinCmd = terminal === terminals.cmd;
    const args = useWinCmd ? ['/c', command] : ['-c', command];
    const thread = spawn(terminal, args, {
      stdio: ['inherit', 'pipe', 'pipe'],
    });
    const stdOut = [];
    const stdErr = [];
    thread.stdout.on('data', (data) => {
      stdOut.push(data.toString());
    });
    thread.stderr.on('data', (data) => {
      stdErr.push(data.toString());
    });
    thread.on('close', () => {
      if (stdErr.length) {
        reject(stdErr.join(''));
      }
      resolve(stdOut.join(''));
    });
  });

export const getPlanRevisionsFromGit = async (planFilePath, terminal) => {
  const command =
    terminal === terminals.bash
      ? `git rev-list --pretty="format:%H|%ad" HEAD ${planFilePath}`
      : `git rev-list --pretty="%H|%ad" HEAD ${planFilePath}`;
  const stdOut = await exec(command, terminal);
  const lines = stdOut
    .split(/\r\n|\r|\n/)
    .filter((r) => r?.includes('|'))
    .map((r) => r?.replace(/"/g, '')); //trim extra double quotes in Windows output
  return lines.map((line) => {
    const [version, timeStamp] = line.split('|');
    const utcTimeStamp = new Date(timeStamp).toISOString();
    return { version, timeStamp: utcTimeStamp };
  });
};

export const getPlanFromGit = async (planFilePath, revision, terminal) => {
  return exec(`git show ${revision}:${planFilePath}`, terminal);
};

export const getHistoricalPlansFromGit = async (planFilePath) => {
  try {
    const gitActive = gitAvailable();
    if (!gitActive) {
      throw new Error('git must be installed and in PATH');
    }
    const terminal = getTerminal();
    const revisions = await getPlanRevisionsFromGit(planFilePath, terminal);
    const historicalPlans = await Promise.all(
      revisions.map(async (revision) => {
        const plan = await getPlanFromGit(
          planFilePath,
          revision.version,
          terminal,
        );
        const version = hash(plan);
        return {
          revision,
          version,
          plan,
        };
      }),
    );

    return {
      historicalPlans,
    };
  } catch (error) {
    console.error(chalk.red(error));
    throw new Error('Unable to fetch plan.json history from git');
  }
};
