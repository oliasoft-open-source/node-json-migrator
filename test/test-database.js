import pgPromise from 'pg-promise';
import { newDb } from 'pg-mem';
import { createMigrationsTables } from '../src/database/database';

export const entity = 'animals';

const pgp = pgPromise({});
export const pgpHelpers = pgp.helpers;

export const getEmptyTestDatabase = async () => {
  const database = await newDb().adapters.createPgPromise();
  database.connect();
  return database;
};

const mock = newDb();
let backup;
let database;
let databaseInitialized = false;

export const getTestDatabase = async () => {
  if (!databaseInitialized) {
    databaseInitialized = true;
    database = await mock.adapters.createPgPromise();
    database.connect();
    await createMigrationsTables(database, entity);
    backup = mock.backup();
  } else {
    backup.restore();
  }
  return database;
};
