import toMatchStringIgnoringFormatting from './match-string-ignoring-formatting/match-string-ignoring-formatting';

expect.extend({
  toMatchStringIgnoringFormatting,
});
