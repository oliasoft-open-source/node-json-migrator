module.exports = {
  moduleFileExtensions: ['js'],
  moduleDirectories: ['node_modules'],
  modulePathIgnorePatterns: ['__testdata__'],
  collectCoverageFrom: [
    /*
      Test code coverage rules:
        - src directory
        - all .js files except *.spec.js and *.test.js
     */
    '**/src/**/!(*.spec|*.test).js',
    //exclude test directories
    '!**/__tests__/**',
    '!**/__test__/**',
    '!**/__testdata__/**',
  ],
  coverageReporters: ['html', 'json', 'cobertura', 'text'],
  coverageDirectory: './test/coverage',
  setupFilesAfterEnv: ['./test/setup.js'],
};
