import path from 'path';
import glob from 'glob-promise';
import { sortBy } from 'lodash';

const matchSkip = /(.skip|.skip\d+).js$/g;

/**
 * Sorts migration file paths
 *
 * @param {Array<String>} filePaths
 * @returns {Array<String>} sorted files
 */

export const sortFilePathsByFilename = (filePaths) =>
  sortBy(filePaths, (file) => {
    return path.parse(file).base;
  });

/**
 * Gets paths to entity migrations in a directory
 *
 * @param {String} pathToMigrations
 * @returns {Promise<Array<String>>} matching migration filePaths
 */

export const getMigrationFilePaths = async (pathToMigrations) => {
  const filePath = (pattern) => `${pathToMigrations}${pattern}`;
  const filePaths = await glob(filePath('/**/*.js'), {
    ignore: [
      filePath('/**/*.skip?([0-9]*).js'),
      filePath('/**/index.js'),
      filePath('/**/plan.json'),
      filePath('/**/*.bundle.js'),
      filePath('/**/*.test.js'),
      filePath('/**/*.spec.js'),
      filePath('/**/__tests__/*'),
      filePath('/**/__test__/*'),
    ],
  });
  return sortFilePathsByFilename(filePaths);
};

/**
 * Gets paths to skipped migrations in a directory
 *
 * @param {String} pathToMigrations
 * @returns {Promise<Array<String>>} skipped migration filePaths
 */

export const getSkippedFilePaths = async (pathToMigrations) => {
  const filePath = (pattern) => `${pathToMigrations}${pattern}`;
  const filePaths = await glob(filePath('/**/*.skip?([0-9]*).js'), {
    ignore: [
      filePath('/**/index.js'),
      filePath('/**/plan.json'),
      filePath('/**/*.bundle.js'),
      filePath('/**/*.test.js'),
      filePath('/**/*.spec.js'),
      filePath('/**/__tests__/*'),
      filePath('/**/__test__/*'),
    ],
  });
  return sortFilePathsByFilename(filePaths);
};

export const getUniqueSkippedFileNames = (skippedPaths) => {
  const fileNames = skippedPaths.map((p) =>
    path.parse(p).base.replace(matchSkip, '.js'),
  );
  return Array.from(new Set(fileNames));
};
