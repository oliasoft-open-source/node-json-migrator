import {
  exec,
  commandAvailable,
  getTerminal,
  getPlanRevisionsFromGit,
  getPlanFromGit,
  getHistoricalPlansFromGit,
  terminals,
  isWindows,
} from './git';

const terminal = getTerminal();

describe('Git', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  describe('exec', () => {
    it('executes a command and resolves upon success', async () => {
      const response = await exec('echo testing command', terminal);
      expect(response).toContain('testing command');
    });
    it('executes a command and rejects upon failure', async () => {
      const badCommand = isWindows ? 'dir foobar' : 'ls foobar';
      const badResponses = ['File Not Found', 'No such file or directory']; //depends on platform, match any
      const regex = new RegExp(`.*(${badResponses.join('|')}).*`, 'i');
      await expect(exec(badCommand, terminal)).rejects.toMatch(regex);
    });
  });
  describe('commandAvailable', () => {
    it('checks whether a command is available', async () => {
      const git = commandAvailable('git');
      const monkeyBananas = commandAvailable('monkey-bananas');
      expect(git).toBe(true);
      expect(monkeyBananas).toBe(false);
    });
  });
  describe('getTerminal', () => {
    it('checks which terminal type to use (bash or cmd.exe on Windows)', async () => {
      const terminalType = await getTerminal();
      expect(Object.values(terminals)).toContain(terminalType);
    });
  });
  describe('getPlanRevisionsFromGit', () => {
    it('gets revisions (commit hashes) from git for plan.json', async () => {
      const response = await getPlanRevisionsFromGit(
        'test/__testdata__/git/plan.json',
        terminal,
      );
      //these are real commits to the test file that we made in this repo
      expect(response).toStrictEqual([
        {
          version: 'a6ee2870f74d74b48be39699a3acd63eb44e6723',
          timeStamp: '2021-11-12T12:22:43.000Z',
        },
        {
          version: '4680d2320ac794cd26d4aa2a6605e0822ad3fbe6',
          timeStamp: '2021-11-12T12:21:25.000Z',
        },
        {
          version: '82a14b9531955028e5e61922eb4559391830055c',
          timeStamp: '2021-11-12T12:20:46.000Z',
        },
        {
          version: '68821762d944197cdc6c4bde1680309a82dbc645',
          timeStamp: '2021-11-12T12:18:49.000Z',
        },
      ]);
    });
    it('rejects when unable to get revisions', () => {
      expect(
        getPlanRevisionsFromGit('path/to/non-existent/plan.json', terminal),
      ).rejects.toContain('unknown revision or path not in the working tree');
    });
  });
  describe('getPlanFromGit', () => {
    it('gets a historical plan file for a given revision', async () => {
      const response = await getPlanFromGit(
        'test/__testdata__/git/plan.json',
        '82a14b9531955028e5e61922eb4559391830055c',
        terminal,
      );
      expect(response).toMatchStringIgnoringFormatting(`[
        {
          "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
          "fileName": "add-panda.js",
          "sequence": "1"
        }
      ]`);
    });
    it('rejects when unable to get a revision', () => {
      expect(
        getPlanFromGit(
          'test/__testdata__/git/plan.json',
          'terriblecommithash',
          terminal,
        ),
      ).rejects.toMatch(/invalid object name/i); //format varies by OS
    });
  });
  describe('getHistoricalPlansFromGit', () => {
    it('gets revisions (commit hashes) from git for plan.json', async () => {
      const { historicalPlans } = await getHistoricalPlansFromGit(
        'test/__testdata__/git/plan.json',
      );
      expect(historicalPlans[1].version).toBe(
        '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161',
      );
      expect(historicalPlans[1].plan).toMatchStringIgnoringFormatting(`[
        {
          "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
          "fileName": "add-panda.js",
          "sequence": "1"
        },
        {
          "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
          "fileName": "add-zebra.js",
          "sequence": "2"
        }
      ]`);
    });
    it('throws error when unable to get revisions', async () => {
      const consoleErrorSpy = jest.spyOn(console, 'error');
      await expect(
        getHistoricalPlansFromGit('path/to/non-existent/plan.json'),
      ).rejects.toThrow('Unable to fetch plan.json history from git');
      expect(consoleErrorSpy).toHaveBeenCalledTimes(1);
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringContaining(
          'unknown revision or path not in the working tree',
        ),
      );
    });
  });
});
