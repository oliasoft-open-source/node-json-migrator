import {
  getMigrationFilePaths,
  getSkippedFilePaths,
  getUniqueSkippedFileNames,
  sortFilePathsByFilename,
} from './glob';

describe('Glob', () => {
  describe('getMigrationFilePaths', () => {
    it('returns .js files but excludes test files', async () => {
      const mockFiles = './test/__testdata__/glob-tests';
      const files = await getMigrationFilePaths(mockFiles);
      expect(files).toBeInstanceOf(Array);
      expect(files).toContain('./test/__testdata__/glob-tests/root-match.js');
      expect(files).toContain(
        './test/__testdata__/glob-tests/subdirectory/inside-subdirectory.js',
      );
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/__tests__/inside-tests-dir.js',
      );
      expect(files).not.toContain('./test/__testdata__/glob-tests/plan.json');
      expect(files).not.toContain('./test/__testdata__/glob-tests/index.js');
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/root-match.test.js',
      );
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/subdirectory/__test__/nested-test.js',
      );
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/subdirectory/inside-subdirectory.spec.js',
      );
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/subdirectory/inside-subdirectory.bundle.js',
      );
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/subdirectory/inside-subdirectory.skip2.js',
      );
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/subdirectory/inside-subdirectory.skip21000.js',
      );
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/root-skipped.skip.js',
      );
    });
  });
  describe('getSkippedFilePaths', () => {
    it('returns .skipN.js files', async () => {
      const mockFiles = './test/__testdata__/glob-tests';
      const files = await getSkippedFilePaths(mockFiles);
      expect(files).toStrictEqual([
        './test/__testdata__/glob-tests/subdirectory/inside-subdirectory.skip2.js',
        './test/__testdata__/glob-tests/subdirectory/inside-subdirectory.skip21000.js',
        './test/__testdata__/glob-tests/root-skipped.skip.js',
      ]);
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/root-match.js',
      );
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/subdirectory/inside-subdirectory.js',
      );
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/__tests__/inside-tests-dir.js',
      );
      expect(files).not.toContain('./test/__testdata__/glob-tests/export.js');
      expect(files).not.toContain('./test/__testdata__/glob-tests/index.js');
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/root-match.test.js',
      );
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/subdirectory/__test__/nested-test.js',
      );
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/subdirectory/inside-subdirectory.spec.js',
      );
      expect(files).not.toContain(
        './test/__testdata__/glob-tests/subdirectory/inside-subdirectory.bundle.js',
      );
    });
  });
  describe('getUniqueSkippedFileNames', () => {
    it('returns unique skipped file names', async () => {
      const skippedFilePaths = [
        'path/to/migrations/foo.skip.js',
        'path/to/migrations/nested/bar.skip.js',
        'path/to/migrations/baz.skip.js',
        'path/to/migrations/baz.skip2.js',
        'path/to/migrations/nested/baz.skip256789.js',
      ];
      const skippedFileNames = getUniqueSkippedFileNames(skippedFilePaths);
      expect(skippedFileNames).toStrictEqual(['foo.js', 'bar.js', 'baz.js']);
    });
  });
  describe('sortFiles', () => {
    it('sorts files based on filename', async () => {
      //sort purely by filename, discounting directory hierarchy
      const unsortedFiles = [
        './server/pg/dataset-migrations/2020-03-29T101010-foo.js',
        './server/pg/dataset-migrations/verify-and-fix-legacy/1-verify-and-fix-legacy-2015-07-10.js',
      ];
      const sortedFiles = [
        './server/pg/dataset-migrations/verify-and-fix-legacy/1-verify-and-fix-legacy-2015-07-10.js',
        './server/pg/dataset-migrations/2020-03-29T101010-foo.js',
      ];
      const result = sortFilePathsByFilename(unsortedFiles);
      expect(result).toStrictEqual(sortedFiles);
    });
  });
});
