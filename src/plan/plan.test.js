import path from 'path';
import mockFs from 'mock-fs';
import { promises as fs } from 'fs';
import * as moduleFromString from 'module-from-string';
import {
  readPlan,
  parsePlan,
  sortPlanEntries,
  writePlan,
  getNextSequenceString,
  formatJSON,
  loadMigrationFunctions,
} from './plan';
import { getPlannedVersion } from './planned-version';
import { pgpHelpers, entity, getTestDatabase } from '../../test/test-database';
import * as plan from './plan';

const fsMockModules = {
  node_modules: mockFs.load(path.resolve(__dirname, '../../node_modules')),
};

jest.mock('../database/database', () => ({
  __esModule: true,
  ...jest.requireActual('../database/database'),
  migrationTablesExist: jest.fn().mockImplementation(() => {
    return true;
  }),
}));

const directory = 'path/to/migrations';
let database;
let config;

describe('Plan', () => {
  beforeEach(() => {
    mockFs.restore();
  });
  describe('JSON.stringify', () => {
    it('output order is deterministic', () => {
      /*
       - JSON.stringify order used to not be deterministic
       - it now is according to https://github.com/nodejs/node/issues/15628#issuecomment-332588533
       - alternative options:
       - https://www.npmjs.com/package/safe-stable-stringify
       - https://www.npmjs.com/package/json-stable-stringify
       */
      const input = [
        { a: 1, c: 3, b: 2 },
        { c: 6, a: 4, b: 6 },
      ];
      const output = JSON.stringify(input, null, 2);
      expect(output).toMatchStringIgnoringFormatting(`[
        { "a": 1, "c": 3, "b": 2 },
        { "c": 6, "a": 4, "b": 6 }
      ]`);
    });
  });
  describe('getNextSequenceString', () => {
    it('gets the next sequence string from the filesystem', async () => {
      const sortedPlannedMigrations = [
        {
          sequence: '1',
        },
        {
          sequence: '2',
        },
      ];
      const nextSequenceString = getNextSequenceString(sortedPlannedMigrations);
      expect(nextSequenceString).toBe('3');
    });
    it('handles when there are sub-sequences', async () => {
      const sortedPlannedMigrations = [
        {
          sequence: '1.2.3',
        },
        {
          sequence: '56.4.5',
        },
      ];
      const nextSequenceNumber = getNextSequenceString(sortedPlannedMigrations);
      expect(nextSequenceNumber).toBe('57');
    });
    it('returns 1 when there are no migrations', async () => {
      const sortedPlannedMigrations = [];
      const nextSequenceNumber = getNextSequenceString(sortedPlannedMigrations);
      expect(nextSequenceNumber).toBe('1');
    });
  });
  describe('readPlan', () => {
    afterEach(() => {
      mockFs.restore();
    });
    it('reads plan.json', async () => {
      const inputPlan = `[{
        "fileHash": "d95acf5d48980b5fece175a7022e174792476b04221ac498b008ff3dc04779e5",
        "fileName": "add-animals.js",
        "sequence": "1"
      }]`;
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': inputPlan,
        },
      });
      const rawPlan = await readPlan(directory);
      expect(typeof rawPlan).toBe('string');
      expect(rawPlan).toStrictEqual(formatJSON(inputPlan));
    });
    it('throws if plan.json cannot be parsed', async () => {
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': `{badJSON$`,
        },
      });
      await expect(readPlan(directory)).rejects.toThrow(
        'Invalid JSON for migrator plan.js (unable to parse file)',
      );
    });
  });
  describe('parsePlan', () => {
    afterEach(() => {
      mockFs.restore();
    });
    it('returns empty array when input is empty array', async () => {
      const inputPlan = '[]';
      const plannedMigrations = parsePlan(inputPlan);
      expect(plannedMigrations.length).toBe(0);
      expect(plannedMigrations).toStrictEqual([]);
    });
    it('throws error when input is not JSON', async () => {
      const inputPlan = '|[malformedJson}';
      expect(() => {
        parsePlan(inputPlan);
      }).toThrow('Invalid JSON for migrator plan.js');
    });
    it('throws error when index file JSON does not match schema', async () => {
      const inputPlan = '[{"name": "Dr Alpaca"}]';
      expect(() => {
        parsePlan(inputPlan);
      }).toThrow('Invalid JSON for migrator plan.js');
    });
  });
  describe('sortPlan', () => {
    it('correctly sorts by sequence number', () => {
      const unsortedPlan = [
        { fileHash: '', fileName: 'D10', sequence: '4.10.10.1' },
        { fileHash: '', fileName: 'G', sequence: '7' },
        { fileHash: '', fileName: 'A', sequence: '1' },
        { fileHash: '', fileName: 'C', sequence: '3' },
        { fileHash: '', fileName: 'I', sequence: '9' },
        { fileHash: '', fileName: 'D1', sequence: '4' },
        { fileHash: '', fileName: 'D2', sequence: '4.1' },
        { fileHash: '', fileName: 'B', sequence: '2' },
        { fileHash: '', fileName: 'D5', sequence: '4.9' },
        { fileHash: '', fileName: 'D3', sequence: '4.1.1' },
        { fileHash: '', fileName: 'D4', sequence: '4.2' },
        { fileHash: '', fileName: 'D9', sequence: '4.10.10' },
        { fileHash: '', fileName: 'D6', sequence: '4.10' },
        { fileHash: '', fileName: 'D7', sequence: '4.10.1' },
        { fileHash: '', fileName: 'D11', sequence: '4.10.10.9' },
        { fileHash: '', fileName: 'F', sequence: '6' },
        { fileHash: '', fileName: 'D8', sequence: '4.10.9' },
        { fileHash: '', fileName: 'K', sequence: '11' },
        { fileHash: '', fileName: 'D12', sequence: '4.10.10.11' },
        { fileHash: '', fileName: 'D13', sequence: '4.12' },
        { fileHash: '', fileName: 'E', sequence: '5' },
        { fileHash: '', fileName: 'H', sequence: '8' },
        { fileHash: '', fileName: 'J', sequence: '10' },
      ];
      const expectedSortedPlan = [
        { fileHash: '', fileName: 'A', sequence: '1' },
        { fileHash: '', fileName: 'B', sequence: '2' },
        { fileHash: '', fileName: 'C', sequence: '3' },
        { fileHash: '', fileName: 'D1', sequence: '4' },
        { fileHash: '', fileName: 'D2', sequence: '4.1' },
        { fileHash: '', fileName: 'D3', sequence: '4.1.1' },
        { fileHash: '', fileName: 'D4', sequence: '4.2' },
        { fileHash: '', fileName: 'D5', sequence: '4.9' },
        { fileHash: '', fileName: 'D6', sequence: '4.10' },
        { fileHash: '', fileName: 'D7', sequence: '4.10.1' },
        { fileHash: '', fileName: 'D8', sequence: '4.10.9' },
        { fileHash: '', fileName: 'D9', sequence: '4.10.10' },
        { fileHash: '', fileName: 'D10', sequence: '4.10.10.1' },
        { fileHash: '', fileName: 'D11', sequence: '4.10.10.9' },
        { fileHash: '', fileName: 'D12', sequence: '4.10.10.11' },
        { fileHash: '', fileName: 'D13', sequence: '4.12' },
        { fileHash: '', fileName: 'E', sequence: '5' },
        { fileHash: '', fileName: 'F', sequence: '6' },
        { fileHash: '', fileName: 'G', sequence: '7' },
        { fileHash: '', fileName: 'H', sequence: '8' },
        { fileHash: '', fileName: 'I', sequence: '9' },
        { fileHash: '', fileName: 'J', sequence: '10' },
        { fileHash: '', fileName: 'K', sequence: '11' },
      ];
      const sortedPlan = sortPlanEntries(unsortedPlan);
      expect(sortedPlan).toStrictEqual(expectedSortedPlan);
      expect(JSON.stringify(sortedPlan)).toBe(
        JSON.stringify(expectedSortedPlan),
      );
    });
    it('sorts the plan by sequence number and sorts properties', () => {
      const unsortedPlan = [
        { fileHash: 'w', fileName: 'foo', sequence: '2' },
        { fileName: 'bar', fileHash: 'x', sequence: '1' },
        { sequence: '3.1', fileName: 'baz', fileHash: 'y' },
        { fileName: 'qux', sequence: '1.1.2', fileHash: 'z' },
      ];
      const expectedSortedPlan = [
        { fileHash: 'x', fileName: 'bar', sequence: '1' },
        { fileHash: 'z', fileName: 'qux', sequence: '1.1.2' },
        { fileHash: 'w', fileName: 'foo', sequence: '2' },
        { fileHash: 'y', fileName: 'baz', sequence: '3.1' },
      ];
      const sortedPlan = sortPlanEntries(unsortedPlan);
      expect(sortedPlan).toStrictEqual(expectedSortedPlan);
      expect(JSON.stringify(sortedPlan)).toBe(
        JSON.stringify(expectedSortedPlan),
      );
    });
  });
  describe('getPlannedVersion', () => {
    beforeEach(async () => {
      jest.clearAllMocks();
      database = await getTestDatabase();
      config = {
        directory,
        database,
        entity,
        pgpHelpers,
      };
    });
    afterEach(() => {
      mockFs.restore();
    });
    it('gets the planned migrations', async () => {
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            }
          ]`,
          'add-panda': {
            'add-panda.js':
              'export default (payload) => ({...payload, panda: "Ralph"});',
          },
        },
      });
      const plannedVersion = await getPlannedVersion({ config });
      expect(plannedVersion).toBe(
        'c9005453332d336b8667ca748c5c6e536ff16cb6568711e94d0c0e07c97d0088',
      );
    });
    it('returns cached version without calling getPlannedMigrations when the plan is unchanged', async () => {
      const planSpy = jest.spyOn(plan, 'getPlannedMigrations');
      const moduleFromStringSpy = jest.spyOn(
        moduleFromString,
        'importFromString',
      );
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
          'add-panda': {
            'add-panda.js':
              'export default (payload) => ({...payload, panda: "Ralph"});',
          },
          'add-zebra': {
            'add-zebra.js':
              'export default (payload) => ({...payload, zebra: "Lachlan"});',
          },
        },
      });
      const plannedVersion = await getPlannedVersion({ config });
      expect(plannedVersion).toBe(
        '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161',
      );
      expect(planSpy).toHaveBeenCalledTimes(1);
      const plannedVersionAgain = await getPlannedVersion({ config });
      expect(plannedVersionAgain).toBe(
        '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161',
      );
      expect(planSpy).toHaveBeenCalledTimes(1);
      expect(moduleFromStringSpy).not.toHaveBeenCalled();
    });
  });
  describe('writePlan', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    afterEach(() => {
      mockFs.restore();
    });
    it('writes the index file', async () => {
      jest.spyOn(fs, 'writeFile');
      mockFs({
        ...fsMockModules,
        [directory]: {
          'plan.json': '{}',
        },
      });
      const nextPlan = `
        [{
          "fileHash": "0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2",
          "fileName": "add-animals.js",
          "sequence": "1",
        }]
      `;
      await writePlan(directory, nextPlan);
      expect(fs.writeFile).toHaveBeenCalledTimes(1);
      const planFile = await fs.readFile(`${directory}/plan.json`, {
        encoding: 'utf8',
      });
      expect(planFile).toBe(nextPlan);
    });
  });
  describe('loadMigrationFunctions', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    afterEach(() => {
      mockFs.restore();
    });
    it('loads migration functions from string import or cache when applicable', async () => {
      const moduleFromStringSpy = jest.spyOn(
        moduleFromString,
        'importFromString',
      );
      const migrations = [
        {
          fileHash:
            '4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c',
          fileName: 'add-panda.js',
          script: 'export default (payload) => ({...payload, panda: "Ralph"});',
          sequence: '1',
        },
      ];
      const activeVersion = 'ABC';
      const migrationsWithFunctions = await loadMigrationFunctions(
        migrations,
        activeVersion,
      );
      expect(typeof migrationsWithFunctions[0].migrator).toBe('function');
      expect(moduleFromStringSpy).toHaveBeenCalledTimes(1);

      //Load a second time (hits cache)
      const migrationsWithFunctions2 = await loadMigrationFunctions(
        migrations,
        activeVersion,
      );
      expect(typeof migrationsWithFunctions2[0].migrator).toBe('function');
      expect(moduleFromStringSpy).toHaveBeenCalledTimes(1);

      //Load a third time (invalidate sorted migrations, regenerate cache)
      const nextActiveVersion = 'ABC';
      const migrationsWithFunctions3 = await loadMigrationFunctions(
        migrations.map((m) => ({
          ...m,
          fileHash:
            '3926ade330768a0df22d6b1519cd62821a7f5bbf2baa80615870fd5372785423',
          script: 'export default (payload) => payload;',
        })),
        nextActiveVersion,
      );
      expect(typeof migrationsWithFunctions3[0].migrator).toBe('function');
      expect(moduleFromStringSpy).toHaveBeenCalledTimes(2);

      //Load a fourth time (invalidate plan.json version, regenerate cache)
      const finalActiveVersion = 'DEF';
      await loadMigrationFunctions(migrations, finalActiveVersion);
      const migrationsWithFunctions5 = await loadMigrationFunctions(
        migrations,
        finalActiveVersion,
      );
      expect(typeof migrationsWithFunctions5[0].migrator).toBe('function');
      expect(moduleFromStringSpy).toHaveBeenCalledTimes(3);
    });
  });
});
