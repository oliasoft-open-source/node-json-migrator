export const migrationTablesExist = (db, entity) => {
  const query = `
    SELECT EXISTS (
      SELECT FROM information_schema.tables
      WHERE table_schema = 'public'
      AND table_name = '${entity}_migrations'
    )
  `;
  return db.any(query);
};

export const createMigrationsTables = (db, entity) => {
  const query = `
    CREATE TABLE IF NOT EXISTS ${entity}_migrations (
      file_hash TEXT NOT NULL PRIMARY KEY,
      file_name TEXT NOT NULL,
      sequence TEXT NOT NULL,
      created_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      UNIQUE (file_name),
      UNIQUE (sequence)
    );
    CREATE TABLE IF NOT EXISTS ${entity}_versions (
      version TEXT NOT NULL PRIMARY KEY,
      plan TEXT NOT NULL,
      created_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    );
  `;
  return db.any(query);
};

export const getMigrationRecords = (db, entity) => {
  const query = `
    SELECT file_hash, file_name, sequence FROM ${entity}_migrations
    ORDER BY sequence
  `;
  return db.manyOrNone(query).then((data) =>
    data.map((r) => ({
      fileHash: r.file_hash,
      fileName: r.file_name,
      sequence: r.sequence,
    })),
  );
};

export const insertMigrationsRecords = (db, pgpHelpers, entity, migrations) => {
  if (migrations.length) {
    const toInsert = migrations
      .map((m) => ({
        file_name: m.fileName,
        file_hash: m.fileHash,
        sequence: m.sequence,
      }))
      .filter((m) => m.file_hash !== null); //can't insert null file hash
    const cs = new pgpHelpers.ColumnSet(
      ['file_name', 'file_hash', 'sequence'],
      {
        table: `${entity}_migrations`,
      },
    );
    const onConflict = ' ON CONFLICT(file_name) DO NOTHING';
    const query = pgpHelpers.insert(toInsert, cs) + onConflict;
    return db.none(query);
  }
};

export const replaceMigrationsRecords = async (
  db,
  pgpHelpers,
  entity,
  migrations,
) => {
  /*
    OW-9043: Table is used for change detection/tracking. DELETE ALL not
    TRUNCATE here so that fewer permissions are needed in downstream
    environments.
   */
  const truncate = `DELETE FROM ${entity}_migrations`;
  await db.none(truncate);
  await insertMigrationsRecords(db, pgpHelpers, entity, migrations);
};

export const insertVersions = (db, pgpHelpers, entity, records) => {
  const cs = new pgpHelpers.ColumnSet(['version', 'plan'], {
    table: `${entity}_versions`,
  });
  const onConflict = ' ON CONFLICT(version) DO NOTHING';
  const query = pgpHelpers.insert(records, cs) + onConflict;
  return db.none(query);
};

export const getPlanFromVersion = (db, entity, version) => {
  const query = `
    SELECT plan FROM ${entity}_versions
    WHERE version=$1;
  `;
  return db.oneOrNone(query, [version], (r) => r?.plan || null);
};

export const getVersions = async (db, entity) => {
  const query = `
    SELECT
       version,
       plan
    FROM ${entity}_versions ORDER BY created_on DESC
  `;
  return db.any(query);
};
