let cachedPlannedVersion = null;

let cachedMigratorFunctions = [];

export const getCachedPlannedVersion = () => cachedPlannedVersion;

export const setCachedPlannedVersion = (version) => {
  cachedPlannedVersion = version;
};

export const getCachedMigratorFunctions = () => cachedMigratorFunctions;

export const setCachedMigratorFunctions = (scripts) => {
  cachedMigratorFunctions = scripts;
};

export const cacheHasAllMigrationFunctions = ({
  sortedMigrationEntries,
  cachedMigrationEntries,
}) =>
  sortedMigrationEntries.every((migration) =>
    cachedMigrationEntries.some(
      (cached) =>
        cached.fileHash === migration.fileHash &&
        typeof cached.migrator === 'function',
    ),
  );
