import chalk from 'chalk';
import { getPlannedMigrations } from '../plan/plan';
import { getExecutedMigrationsFromVersion } from '../executed/executed';

const shortHash = (hash) => (hash ? hash.substring(0, 8) : '');

export const getPendingMigrations = ({
  plannedMigrations,
  executedMigrations,
  printPendingFileNames = false,
  force,
}) => {
  const pendingMigrations = plannedMigrations.filter((m) => {
    const executed = executedMigrations.find((e) => e?.fileName === m.fileName);
    const skippedFileHashes = m?.skippedFileHashes;
    const isSkippedAndPreviouslyExecuted = skippedFileHashes?.includes(
      executed?.fileHash,
    );
    /*
      - always execute migrations that have never been executed
      - re-execute migrations only when all are true:
        - fileHash has changed
        - force flag is active
        - the migration was not previously executed and skipped (see .skip feature for patching migration bugs)
     */
    return (
      executed === undefined ||
      (executed?.fileHash !== m?.fileHash &&
        force &&
        !isSkippedAndPreviouslyExecuted)
    );
  });
  if (printPendingFileNames) {
    pendingMigrations.forEach((m) => {
      console.log(chalk.gray(`  ${m.fileName} ${shortHash(m?.fileHash)}`));
    });
  }
  return pendingMigrations.map((m) => ({
    fileName: m.fileName,
    migrator: m.migrator,
  }));
};

export const getPendingMigrators = async ({ config }) => {
  const {
    directory,
    database,
    pgpHelpers,
    entity,
    version,
    plan,
    printPendingFileNames,
    force,
  } = config;

  const { plannedMigrations, nextVersion } =
    plan ||
    (await getPlannedMigrations({
      config,
    }));

  const executedMigrations = database
    ? await getExecutedMigrationsFromVersion(
        database,
        pgpHelpers,
        entity,
        directory,
        version,
        nextVersion,
        plannedMigrations,
      )
    : [];

  const pendingMigrators = getPendingMigrations({
    plannedMigrations,
    executedMigrations,
    printPendingFileNames,
    force,
  });

  return {
    pendingMigrators,
    nextVersion:
      plannedMigrations.length < executedMigrations.length
        ? version
        : nextVersion,
  };
};
