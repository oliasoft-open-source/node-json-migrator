# Node JSON Migrator Release Notes

## 2.3.10

- Upgrade `esbuild` to a non-vulnerable version via upgrading minor version of `module-from-string` ([OW-18954](https://oliasoft.atlassian.net/browse/OW-18954))

## 2.3.9

- Upgrade to Node version 22 ([OW-18913](https://oliasoft.atlassian.net/browse/OW-18913))

## 2.3.8

- Fix cache-busting condition for cached imports of migration functions ([OW-15651](https://oliasoft.atlassian.net/browse/OW-15651))

## 2.3.7

- Fix memory leak bug when repeating calls to `migrate()` ([OW-15584](https://oliasoft.atlassian.net/browse/OW-15584))

## 2.3.6

- Fix mattermost url and changed channel name to Release bots ([OW-14761](https://oliasoft.atlassian.net/browse/OW-14761))

## 2.3.5

- Fix bug where `version` wrongly got downgraded when rolling back to old `plan.json` ([OW-13375](https://oliasoft.atlassian.net/browse/OW-13375))

## 2.3.4

- Fix bug applying local changes with the `:force` flag ([OW-11901](https://oliasoft.atlassian.net/browse/OW-11901))

## 2.3.3

- Fix release CI/CD pipeline ([OW-11691](https://oliasoft.atlassian.net/browse/OW-11691))

## 2.3.2

- handle build step in CI/CD pipelines instead of committing `dist` files ([OW-11002](https://oliasoft.atlassian.net/browse/OW-11002))
- move some common packages to `peerDependencies`

## 2.3.1

- fix support for git history search on Windows ([OW-8747](https://oliasoft.atlassian.net/browse/OW-8747))
- fix SQL error `Cannot generate an INSERT from an empty array` ([OW-10581](https://oliasoft.atlassian.net/browse/OW-10581))

## 2.3.0

- switch from npm to yarn (to match other repos)
- Add optional CI pipeline that automates publishing of beta releases ([OW-10003](https://oliasoft.atlassian.net/browse/OW-10003))

## 2.2.0

- first publish of release notes
