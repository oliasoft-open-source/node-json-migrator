import { pipe } from './pipe';

describe('Migrator', () => {
  describe('pipe', () => {
    it('executes a sequence of migrations on a json input', () => {
      const migrations = [
        {
          fileName: 'script-1.js',
          migrator: (input) => ({ ...input, foo: 'bar' }),
        },
        {
          fileName: 'script-2.js',
          migrator: (input) => ({ ...input, baz: 'qux' }),
        },
      ];
      const payload = {};
      const nextJson = pipe(migrations, payload);
      expect(nextJson).toStrictEqual({
        foo: 'bar',
        baz: 'qux',
      });
    });
    it('throws when migrator is not a function', () => {
      const migrations = [
        {
          fileName: 'script-1.js',
          migrator: (input) => ({ ...input, foo: 'bar' }),
        },
        {
          fileName: 'script-2.js',
          migrator: 'Bad function',
        },
      ];
      const payload = {};
      expect(() => {
        pipe(migrations, payload);
      }).toThrow('Expected a function');
    });
    it('throws with fileName when migrator throws exception', () => {
      const migrations = [
        {
          fileName: 'script-1.js',
          migrator: (input) => ({ ...input, foo: 'bar' }),
        },
        {
          fileName: 'script-2.js',
          migrator: () => {
            throw new Error('Rubbish migration');
          },
        },
      ];
      const payload = {};
      expect(() => {
        pipe(migrations, payload);
      }).toThrow('Unhandled exception in file script-2.js: Rubbish migration');
    });
  });
});
