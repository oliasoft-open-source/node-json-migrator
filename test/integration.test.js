import { migrate } from '../src/migrate/migrate';
import { pgpHelpers, entity, getTestDatabase } from './test-database';

jest.mock('../src/database/database', () => ({
  __esModule: true,
  ...jest.requireActual('../src/database/database'),
  migrationTablesExist: jest.fn().mockImplementation(() => {
    return true;
  }),
}));

describe('Integration', () => {
  describe('migrate', () => {
    it('can invoke migrate function from real files', async () => {
      const database = await getTestDatabase();
      const directory = './test/__testdata__/migrations';
      const payload = {};
      const version = undefined;
      const config = {
        directory,
        database,
        entity,
        version,
        pgpHelpers,
      };
      const { nextPayload, nextVersion } = await migrate({
        payload,
        config,
      });
      expect(nextPayload).toEqual({
        animals: ['Alpaca', 'Highland cow', 'Aardvark'],
      });
      expect(nextVersion).toBe(
        '2b4d6b2a5a35cc0ef431bddabaff9ab3dad7d2abb415ccb79e6d8e4ded751b52',
      );
    });
  });
});
