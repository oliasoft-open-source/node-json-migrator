import { getPendingMigrations } from './pending';

const consoleLogSpy = jest.spyOn(console, 'log');

describe('Migrator', () => {
  describe('getPendingMigrations', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    it('gets pending migrations', () => {
      const plannedMigrations = [
        {
          fileName: 'add-panda.js',
          fileHash: 'A',
          migrator: () => {},
        },
        {
          fileName: 'add-donkey.js',
          fileHash: 'C',
          migrator: () => {},
        },
        {
          fileName: 'add-zebra.js',
          fileHash: 'D',
          migrator: () => {},
        },
      ];
      const executedMigrations = [
        {
          fileName: 'add-panda.js',
          fileHash: 'A',
          migrator: () => {},
        },
        {
          fileName: 'add-donkey.js',
          fileHash: 'B',
          migrator: () => {},
        },
      ];
      const pendingMigrations = getPendingMigrations({
        plannedMigrations,
        executedMigrations,
      });
      expect(pendingMigrations.length).toBe(1);
      expect(typeof pendingMigrations[0].migrator).toBe('function');
      expect(consoleLogSpy).not.toHaveBeenCalled();
    });
    it('gets pending migrations, includes changed files when forced except previously executed and skipped', () => {
      const plannedMigrations = [
        {
          fileName: 'add-panda.js',
          fileHash: 'A',
          migrator: () => {},
        },
        {
          fileName: 'add-donkey.js',
          fileHash: 'D',
          migrator: () => {},
        },
        {
          fileName: 'add-binturong.js',
          fileHash: 'E',
          skippedFileHashes: ['B'],
          migrator: () => {},
        },
        {
          fileName: 'add-zebra.js',
          fileHash: 'F',
          migrator: () => {},
        },
      ];
      const executedMigrations = [
        {
          fileName: 'add-panda.js',
          fileHash: 'A',
          migrator: () => {},
        },
        {
          fileName: 'add-binturong.js',
          fileHash: 'B',
          migrator: () => {},
        },
        {
          fileName: 'add-donkey.js',
          fileHash: 'C',
          migrator: () => {},
        },
      ];
      const pendingMigrations = getPendingMigrations({
        plannedMigrations,
        executedMigrations,
        force: true,
      });
      expect(pendingMigrations.length).toBe(2);
      expect(typeof pendingMigrations[0].migrator).toBe('function');
      expect(typeof pendingMigrations[1].migrator).toBe('function');
      expect(consoleLogSpy).not.toHaveBeenCalled();
    });
    it('prints pending migration names when configured', () => {
      const plannedMigrations = [
        {
          fileName: 'add-panda.js',
          fileHash: 'A',
          migrator: () => {},
        },
        {
          fileName: 'add-donkey.js',
          fileHash: 'C',
          migrator: () => {},
        },
        {
          fileName: 'add-zebra.js',
          fileHash: 'D',
          migrator: () => {},
        },
      ];
      const executedMigrations = [
        {
          fileName: 'add-panda.js',
          fileHash: 'A',
          migrator: () => {},
        },
        {
          fileName: 'add-donkey.js',
          fileHash: 'B',
          migrator: () => {},
        },
      ];
      const pendingMigrations = getPendingMigrations({
        plannedMigrations,
        executedMigrations,
        printPendingFileNames: true,
        force: true,
      });
      expect(pendingMigrations.length).toBe(2);
      expect(typeof pendingMigrations[0].migrator).toBe('function');
      expect(typeof pendingMigrations[1].migrator).toBe('function');
      expect(consoleLogSpy).toHaveBeenCalledTimes(2);
      expect(consoleLogSpy).toHaveBeenCalledWith(
        expect.stringContaining('add-donkey.js C'),
      );
      expect(consoleLogSpy).toHaveBeenCalledWith(
        expect.stringContaining('add-zebra.js D'),
      );
    });
  });
});
