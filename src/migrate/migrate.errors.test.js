import path from 'path';
import mockFs from 'mock-fs';
import { migrate } from './migrate';
import { pgpHelpers, entity, getTestDatabase } from '../../test/test-database';

const directory = 'path/to/migrations';
let database;
let config;

const fsMockModules = {
  node_modules: mockFs.load(path.resolve(__dirname, '../../node_modules')),
};

jest.mock('../database/database', () => ({
  __esModule: true,
  ...jest.requireActual('../database/database'),
  migrationTablesExist: jest.fn().mockImplementation(() => {
    return true;
  }),
}));

describe('Behavioural tests', () => {
  beforeEach(async () => {
    jest.clearAllMocks();
    database = await getTestDatabase();
    config = {
      directory,
      database,
      entity,
      pgpHelpers,
    };
  });
  afterEach(() => {
    mockFs.restore();
  });
  it('includes the migration file name in exception stack traces ', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "00e763c3c08ca3f89eaa8bfc9ac436d239624e4ea34c7bb2e73bfb288272af17",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
        'add-zebra': {
          'add-zebra.js':
            "export default (payload) => {throw new Error('Failure')};",
        },
      },
    });
    const payload = {};
    const version = undefined;
    await expect(
      migrate({
        payload,
        config: {
          ...config,
          version,
        },
      }),
    ).rejects.toThrow('Unhandled exception in file add-zebra.js: Failure');
  });
});
