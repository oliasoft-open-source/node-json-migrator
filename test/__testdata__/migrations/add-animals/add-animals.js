import produce from 'immer';

export default (payload) =>
  produce(payload, (draft) => {
    draft.animals = ['Alpaca', 'Highland cow', 'Aardvark'];
  });
