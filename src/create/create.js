import path from 'path';
import { promises as fsp, promises as fs } from 'fs';
import chalk from 'chalk';
import { validateFileDescription } from '../plan/validator';
import {
  getNextSequenceString,
  readPlan,
  parsePlan,
  writePlan,
} from '../plan/plan';

const templateMigrationFile = `import produce from 'immer';
//other imports not allowed

export default (dataset) => produce(dataset, (draft) => {
  // https://immerjs.github.io/immer/produce#example
});
`;

const templateTestFile = (fileName) => `import migrate from './${fileName}';

describe('describe dataset change', () => {
  test('describe test', () => {
    //arrange
    const dataset = {};

    //act
    const nextDataset = migrate(dataset);

    //assert
  });
});
`;

/**
 * Generate a new dataset migration
 *
 * @param {String} directory path to migrations directory
 * @param {String} description description of change-set / migration
 */

export const createMigration = async (directory, description) => {
  if (!validateFileDescription(description)) {
    throw new Error('Invalid migration description');
  }
  try {
    const directoryFullPath = path.resolve(directory, description);
    const fileName = `${description}.js`;
    const testFileName = `${description}.test.js`;
    const filePath = path.resolve(directoryFullPath, fileName);
    const testFilePath = path.resolve(directoryFullPath, testFileName);
    await fs.mkdir(directoryFullPath, { recursive: true });
    await fsp.writeFile(filePath, templateMigrationFile, { encoding: 'utf8' });
    await fsp.writeFile(testFilePath, templateTestFile(fileName), {
      encoding: 'utf8',
    });
    const rawPlan = await readPlan(directory);
    const plannedMigrations = parsePlan(rawPlan);
    const nextPlannedMigrations = plannedMigrations.concat({
      fileHash: '',
      fileName,
      sequence: getNextSequenceString(plannedMigrations),
    });
    const nextPlan = JSON.stringify(nextPlannedMigrations, null, 2);
    await writePlan(directory, nextPlan);
    console.log(chalk.green(`Created new dataset migration ${fileName} ✓`));
  } catch (error) {
    console.log(chalk.red(error));
    throw new Error('Unable to create migration file');
  }
};
