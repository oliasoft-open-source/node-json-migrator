module.exports = {
  env: {
    es2021: true,
    node: true,
    jest: true,
  },
  extends: ['airbnb/base', 'prettier'],
  parser: '@babel/eslint-parser',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  rules: {
    'consistent-return': 0,
    eqeqeq: 0,
    'import/extensions': 0,
    'import/prefer-default-export': 0,
    'one-var': 0,
    'spaced-comment': 0,
    'arrow-body-style': 0,
    'no-else-return': 0,
    'no-return-assign': 0,
    'no-restricted-syntax': 0,
    'no-console': 0,
    'import/no-extraneous-dependencies': 0,
    'no-param-reassign': 0,
  },
};
