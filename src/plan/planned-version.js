import { hash } from '../hash/hash';
import { getPlannedMigrations, readPlan } from './plan';
import { getCachedPlannedVersion } from './cached-planned-version';

export const getPlannedVersion = async ({ config }) => {
  const { directory } = config;
  const currentPlan = await readPlan(directory);
  const plannedVersion = hash(currentPlan);
  const cachedPlannedVersion = getCachedPlannedVersion();
  return cachedPlannedVersion === plannedVersion
    ? cachedPlannedVersion
    : (
        await getPlannedMigrations({
          config: { ...config, importModule: false },
        })
      ).nextVersion;
};
