import { cacheHasAllMigrationFunctions } from './cached-planned-version';

describe('Cache', () => {
  test('cacheHasAllMigrationFunctions returns true when all migrators are in cache', () => {
    expect(
      cacheHasAllMigrationFunctions({
        sortedMigrationEntries: [
          { fileHash: '8073f7f0-880e-4abb-b698-d64ae3ddd977' },
          { fileHash: 'e54f2381-8afd-4d82-ab9b-fba8e8dbbce4' },
        ],
        cachedMigrationEntries: [
          {
            fileHash: '8073f7f0-880e-4abb-b698-d64ae3ddd977',
            migrator: (a) => a,
          },
          {
            fileHash: 'e54f2381-8afd-4d82-ab9b-fba8e8dbbce4',
            migrator: (b) => b,
          },
        ],
      }),
    ).toBe(true);
  });
  test('cacheHasAllMigrationFunctions returns false when not all migrators are in cache', () => {
    expect(
      cacheHasAllMigrationFunctions({
        sortedMigrationEntries: [
          { fileHash: '8073f7f0-880e-4abb-b698-d64ae3ddd977' },
          { fileHash: 'cd013fc3-932a-4a3e-8483-d034e3e597d7' },
        ],
        cachedMigrationEntries: [
          {
            fileHash: '8073f7f0-880e-4abb-b698-d64ae3ddd977',
            migrator: (a) => a,
          },
          {
            fileHash: 'e54f2381-8afd-4d82-ab9b-fba8e8dbbce4',
            migrator: (b) => b,
          },
        ],
      }),
    ).toBe(false);
  });
});
