declare global {
  namespace jest {
    interface Matchers<R> {
      toMatchStringIgnoringFormatting(expected: string): R
    }
  }
}

export {};
