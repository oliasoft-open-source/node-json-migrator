import { pipe } from '../pipe/pipe';
import { getPendingMigrators } from '../pending/pending';

/**
 * @typedef {Array|Object} Payload
 */

/**
 * @typedef {Object} PlannedMigrations
 * @property {Array<function>} plannedMigrations (planned migrations)
 * @property {string} nextVersion (next payload version)
 */

/**
 * @typedef {Object} Configuration
 * @property {string} directory (path to migrations directory)
 * @property {string} entity (entity name for payload, e.g. dataset)
 * @property {string} [version] (current payload version)
 * @property {boolean} [force] (override validation)
 * @property {boolean} [dry] (don't alter database or files)
 * @property {Object} [database] (connected pg-promise database)
 * @property {Object} [pgpHelpers] (pg-promise helpers)
 * @property {PlannedMigrations} [plan]
 */

/**
 * Migrates a payload
 *
 * There are two ways to use this function:
 * i) function looks up the pending migrations for you (default)
 * ii) pass in pre-fetched pending migrators via config.pending
 *
 * @param {Object} args
 * @param {Payload} args.payload
 * @param {Configuration} args.config
 * @returns Promise<{nextPayload: Payload, nextVersion: string}>
 */

export const migrate = async ({ payload, config }) => {
  const { pendingMigrators, nextVersion } = await getPendingMigrators({
    config,
  });
  return {
    nextPayload: pendingMigrators.length
      ? pipe(pendingMigrators, payload)
      : payload,
    nextVersion,
  };
};
