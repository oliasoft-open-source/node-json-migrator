import mockFs from 'mock-fs';
import { getExecutedMigrationsFromVersion } from './executed';
import { entity, getTestDatabase, pgpHelpers } from '../../test/test-database';

const directory = 'path/to/migrations';

const consoleErrorSpy = jest.spyOn(console, 'error');
const consoleWarnSpy = jest.spyOn(console, 'warn');

describe('Executed Migrations', () => {
  describe('getExecutedMigrationsFromVersion', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    afterEach(() => {
      mockFs.restore();
    });
    it('returns empty array when there is no current version', async () => {
      const db = await getTestDatabase();
      const currentVersion = undefined;
      const executedMigrations = await getExecutedMigrationsFromVersion(
        db,
        pgpHelpers,
        entity,
        directory,
        currentVersion,
      );
      expect(executedMigrations.length).toBe(0);
      expect(executedMigrations).toStrictEqual([]);
      expect(consoleErrorSpy).not.toHaveBeenCalled();
      expect(consoleWarnSpy).not.toHaveBeenCalled();
    });
    it('returns the planned migrations when the current version and planned version are the same', async () => {
      const db = await getTestDatabase();
      const plannedMigrations = [
        {
          fileHash:
            '4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c',
          fileName: 'add-panda.js',
          sequence: '1',
        },
      ];
      const currentVersion =
        '364cf9077bf871d961b41d1e5d6ea8e104ff69649ce206883f24a6db8aca7769';
      const plannedVersion = currentVersion;
      const executedMigrations = await getExecutedMigrationsFromVersion(
        db,
        pgpHelpers,
        entity,
        directory,
        currentVersion,
        plannedVersion,
        plannedMigrations,
      );
      expect(executedMigrations.length).toBe(1);
      expect(executedMigrations[0].fileName).toBe('add-panda.js');
      expect(consoleErrorSpy).not.toHaveBeenCalled();
      expect(consoleWarnSpy).not.toHaveBeenCalled();
    });
    it('returns the historical plan when the version is found in database', async () => {
      const db = await getTestDatabase();
      await db.any(`
        INSERT INTO animals_versions (version, plan)
        VALUES (
          'a64212a5719ad2e0e95805775821922b66e5d0759478fccf642dc8afa4cd7470',
          '[{"fileHash": "0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2", "fileName": "add-panda.js", "sequence": "1"}]'
        )
      `);
      const currentVersion =
        'a64212a5719ad2e0e95805775821922b66e5d0759478fccf642dc8afa4cd7470';
      const executedMigrations = await getExecutedMigrationsFromVersion(
        db,
        pgpHelpers,
        entity,
        directory,
        currentVersion,
      );
      expect(executedMigrations.length).toBe(1);
      expect(executedMigrations[0].fileName).toBe('add-panda.js');
      expect(consoleErrorSpy).not.toHaveBeenCalled();
      expect(consoleWarnSpy).not.toHaveBeenCalled();
    });
    it('returns the historical plan when the version is found in git history', async () => {
      const db = await getTestDatabase();
      const mockGitDirectory = 'test/__testdata__/git';
      const currentVersion =
        '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161';
      const dbRecordsBefore = await db.any('SELECT * FROM animals_versions');
      expect(dbRecordsBefore).toStrictEqual([]);
      const executedMigrations = await getExecutedMigrationsFromVersion(
        db,
        pgpHelpers,
        entity,
        mockGitDirectory,
        currentVersion,
      );
      const dbRecordsAfter = await db.any('SELECT * FROM animals_versions');
      expect(dbRecordsAfter.length).toBe(4);
      expect(dbRecordsAfter[0].version).toBe(
        'af5462b1265365937b14dab1be052e73d1bf218b71a21bf5daebee5a273c1f76',
      );
      expect(executedMigrations.length).toBe(2);
      expect(executedMigrations[0].fileName).toBe('add-panda.js');
      expect(consoleWarnSpy).toHaveBeenCalledTimes(1);
      expect(consoleWarnSpy).toHaveBeenCalledWith(
        expect.stringContaining(
          'Version not found in animals_version table, trying git history instead',
        ),
      );
      expect(consoleErrorSpy).not.toHaveBeenCalled();
    });
    it('throws error when version not found in database nor git history', async () => {
      const db = await getTestDatabase();
      const mockGitDirectory = 'test/__testdata__/git/plan.json';
      const currentVersion =
        'f907352328a21b07bb870b880989d7b7066d05d79e4e1a3824d6e65795508fed';
      await expect(
        getExecutedMigrationsFromVersion(
          db,
          pgpHelpers,
          entity,
          mockGitDirectory,
          currentVersion,
        ),
      ).rejects.toThrow('Unable to fetch plan.json history from git');
      expect(consoleWarnSpy).toHaveBeenCalledTimes(1);
      expect(consoleWarnSpy).toHaveBeenCalledWith(
        expect.stringContaining(
          'Version not found in animals_version table, trying git history instead',
        ),
      );
      expect(consoleErrorSpy).toHaveBeenCalledTimes(1);
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringContaining(
          'unknown revision or path not in the working tree',
        ),
      );
    });
    it('throws when unable to parse plan from database', async () => {
      const db = await getTestDatabase();
      await db.any(`
        INSERT INTO animals_versions (version, plan)
        VALUES (
          'a64212a5719ad2e0e95805775821922b66e5d0759478fccf642dc8afa4cd7470',
          '[horribleJson}'
        )
      `);
      const currentVersion =
        'a64212a5719ad2e0e95805775821922b66e5d0759478fccf642dc8afa4cd7470';
      await expect(
        getExecutedMigrationsFromVersion(
          db,
          pgpHelpers,
          entity,
          directory,
          currentVersion,
        ),
      ).rejects.toThrow(
        'Invalid record in animals_version table (cannot parse JSON)',
      );
      expect(consoleErrorSpy).not.toHaveBeenCalled();
      expect(consoleWarnSpy).not.toHaveBeenCalled();
    });
    it('throws when JSON in database does not match schema', async () => {
      const db = await getTestDatabase();
      await db.any(`
        INSERT INTO animals_versions (version, plan)
        VALUES (
          'a64212a5719ad2e0e95805775821922b66e5d0759478fccf642dc8afa4cd7470',
          '{"foo": 123}'
        )
      `);
      const currentVersion =
        'a64212a5719ad2e0e95805775821922b66e5d0759478fccf642dc8afa4cd7470';
      await expect(
        getExecutedMigrationsFromVersion(
          db,
          pgpHelpers,
          entity,
          directory,
          currentVersion,
        ),
      ).rejects.toThrow(
        'Invalid plan in animals_version table (does not match schema)',
      );
      expect(consoleErrorSpy).toHaveBeenCalledTimes(1);
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringContaining('must be array'),
      );
      expect(consoleWarnSpy).not.toHaveBeenCalled();
    });
  });
});
