import path from 'path';
import { promises as fs } from 'fs';
import mockFs from 'mock-fs';
import * as pipe from '../pipe/pipe';
import { migrate } from './migrate';
import { getPlannedMigrations, readPlan } from '../plan/plan';
import { hash } from '../hash/hash';
import { pgpHelpers, entity, getTestDatabase } from '../../test/test-database';

const directory = 'path/to/migrations';
let database;
let config;
const consoleWarnSpy = jest.spyOn(console, 'warn').mockImplementation(() => {});
const consoleErrorSpy = jest
  .spyOn(console, 'error')
  .mockImplementation(() => {});

const pipeSpy = jest.spyOn(pipe, 'pipe');

const fsMockModules = {
  node_modules: mockFs.load(path.resolve(__dirname, '../../node_modules')),
};

jest.mock('../database/database', () => ({
  __esModule: true,
  ...jest.requireActual('../database/database'),
  migrationTablesExist: jest.fn().mockImplementation(() => {
    return true;
  }),
}));

const addMigrationFile = async (migrationName, fileContent, sequence) => {
  const fileName = `${migrationName}.js`;
  await fs.mkdir(`${directory}/${migrationName}`, { recursive: true });
  await fs.writeFile(
    path.resolve(`${directory}/${migrationName}/${fileName}`),
    fileContent,
    {
      encoding: 'utf8',
    },
  );
  const indexFilePath = path.resolve(`${directory}/plan.json`);
  const indexFile = await fs.readFile(indexFilePath, { encoding: 'utf8' });
  const index = JSON.parse(indexFile);
  const nextIndex = index.concat({
    fileName,
    fileHash: hash(fileContent),
    sequence,
  });
  const nextIndexFile = JSON.stringify(nextIndex, null, 2);
  await fs.writeFile(indexFilePath, nextIndexFile, { encoding: 'utf8' });
};

const deleteMigrationFile = async (migrationName) => {
  const fileName = `${migrationName}.js`;
  await fs.unlink(path.resolve(`${directory}/${migrationName}/${fileName}`));
  const indexFilePath = path.resolve(`${directory}/plan.json`);
  const indexFile = await fs.readFile(indexFilePath, { encoding: 'utf8' });
  const index = JSON.parse(indexFile);
  const nextIndex = index.filter((m) => m.fileName !== fileName);
  const nextIndexFile = JSON.stringify(nextIndex, null, 2);
  await fs.writeFile(indexFilePath, nextIndexFile, { encoding: 'utf8' });
};

describe('Behavioural tests', () => {
  beforeEach(async () => {
    mockFs.restore();
    jest.clearAllMocks();
    database = await getTestDatabase();
    config = {
      directory,
      database,
      entity,
      pgpHelpers,
    };
  });
  it('returns an unmodified payload when there are no migrations', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[]`,
      },
    });
    const payload = {};
    const version = undefined;

    const migratedPayload = await migrate({
      payload,
      config: {
        ...config,
        version,
      },
    });
    expect(migratedPayload.nextPayload).toEqual({});
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
  });
  it('returns a migrated payload when there is a migration', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
      },
    });
    const payload = {};
    const version = undefined;

    const migratedPayload = await migrate({
      payload,
      config: {
        ...config,
        version,
      },
    });
    expect(migratedPayload.nextPayload).toEqual({ panda: 'Ralph' });
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
  });
  it('can fetch the plan once for batched executions', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
      },
    });
    const payload = {};
    const version = undefined;
    const plan = await getPlannedMigrations({ config });
    const migratedPayload = await migrate({
      payload,
      config: {
        ...config,
        version,
        plan,
      },
    });
    expect(migratedPayload.nextPayload).toEqual({ panda: 'Ralph' });
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
  });
  it('can execute multiple migrations', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
      },
    });
    const payload = {};
    const version = undefined;

    const migratedPayload = await migrate({
      payload,
      config: {
        ...config,
        version,
      },
    });
    expect(migratedPayload.nextPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
    });
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
  });
  it('throws error when filenames are badly formatted', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "addPanda.js",
              "sequence": "1"
            }
          ]`,
        addPanda: {
          'addPanda.js':
            'export default (payload) => ({...payload, panda: "Wally"});',
        },
      },
    });
    const payload = {};
    const version = undefined;

    await expect(
      migrate({
        payload,
        config: {
          ...config,
          version,
        },
      }),
    ).rejects.toThrow('Invalid migration filename format (use kebab-case)');
    expect(consoleErrorSpy).toHaveBeenCalledWith(
      expect.stringMatching('addPanda.js'),
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).toHaveBeenCalledTimes(1);
  });
  it('not allowed to modify previous migrations', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Wally"});/*modified*/',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
      },
    });
    const payload = {};
    const version = undefined;

    await expect(
      migrate({
        payload,
        config: {
          ...config,
          version,
        },
      }),
    ).rejects.toThrow(
      'Not allowed to change migration files (for unreleased local work, you can use the `force` option)',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).toHaveBeenCalledTimes(1);
    expect(consoleErrorSpy).toHaveBeenCalledWith(
      expect.stringMatching('add-panda.js'),
    );
  });
  it('does not throw when new migrations have unspecified file hashes', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Wally"});',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
      },
    });
    const payload = {};
    const version = undefined;
    const { nextPayload, nextVersion } = await migrate({
      payload,
      config: {
        ...config,
        version,
      },
    });
    expect(nextVersion).toBe(
      '435edf7a2c3d750db38e8575b26f337a348989529e65b78c6f3dee74571828d9',
    );
    expect(nextPayload).toEqual({ panda: 'Wally', zebra: 'Lachlan' });
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
  });
  it('can .skip a migration with noop (no replacement logic)', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
      },
    });
    const payload = { panda: 'Wally' };
    const version = undefined;

    const { nextPayload, nextVersion } = await migrate({
      payload,
      config: {
        ...config,
        version,
      },
    });
    expect(nextPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
    });
    expect(nextVersion).toBe(
      '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    //now we realised add-panda.js was a mistake and want to replace it with a noop
    await fs.rename(
      `${directory}/add-panda/add-panda.js`,
      `${directory}/add-panda/add-panda.skip.js`,
    );
    await fs.writeFile(
      path.resolve(`${directory}/add-panda/add-panda.js`),
      `export default (payload) => payload;`,
      {
        encoding: 'utf8',
      },
    );

    //check that we can still re-execute the migrations
    const { nextPayload: finalPayload, nextVersion: finalVersion } =
      await migrate({
        payload: nextPayload,
        config: {
          ...config,
          version: nextVersion,
        },
      });
    expect(finalPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan', //no change because this was executed before
    });
    expect(finalVersion).toBe(
      '19f3ffe807e84c9210afa7e58103b9e8864e1c78965d6f484ee690eae194bd5a',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    //final test that the new script executes on an old payload with no version
    const { nextPayload: oldUpgradedPayload, nextVersion: oldUpgradedVersion } =
      await migrate({
        payload: {},
        config: {
          ...config,
        },
      });
    expect(oldUpgradedPayload).toEqual({
      zebra: 'Lachlan',
    });
    expect(oldUpgradedVersion).toBe(
      '19f3ffe807e84c9210afa7e58103b9e8864e1c78965d6f484ee690eae194bd5a',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
  });
  it('.skip of a migration without replacing gives an error', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
      },
    });
    const payload = { panda: 'Wally' };
    const version = undefined;

    const { nextPayload, nextVersion } = await migrate({
      payload,
      config: {
        ...config,
        version,
      },
    });
    expect(nextPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
    });
    expect(nextVersion).toBe(
      '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    //now we skip add-panda.js without replacing it
    await fs.rename(
      `${directory}/add-panda/add-panda.js`,
      `${directory}/add-panda/add-panda.skip.js`,
    );

    //can't run the migrations because we didn't replace the skipped file
    await expect(
      migrate({
        payload: nextPayload,
        config: {
          ...config,
          version: nextVersion,
        },
      }),
    ).rejects.toThrow(
      'Migration files from plan.json are missing from filesystem',
    );
  });
  it('can .skip a migration and replace it (back-fit bugfix) without re-executing, including when forced', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
      },
    });
    const payload = { panda: 'Wally' };
    const version = undefined;

    const { nextPayload, nextVersion } = await migrate({
      payload,
      config: {
        ...config,
        version,
        force: true,
      },
    });
    expect(nextPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
    });
    expect(nextVersion).toBe(
      '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    //now we realised there's a bug in add-panda.js (it overwrites pre-existing pandas)
    //so we replace that script with one with a guard
    await fs.rename(
      `${directory}/add-panda/add-panda.js`,
      `${directory}/add-panda/add-panda.skip.js`,
    );
    await fs.writeFile(
      path.resolve(`${directory}/add-panda/add-panda.js`),
      `export default (payload) => ({
        ...payload,
        panda: payload.panda ? payload.panda + ' Amended'  : "Ralph"
      });`,
      {
        encoding: 'utf8',
      },
    );

    //we want that script not to re-execute
    const { nextPayload: finalPayload, nextVersion: finalVersion } =
      await migrate({
        payload: nextPayload,
        config: {
          ...config,
          version: nextVersion,
          force: true,
        },
      });
    expect(finalPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan', //no change because corrected script does not re-execute
    });
    expect(finalVersion).toBe(
      '97e5ca8e0e484bb4b0572f76b2e9b47eb23ba239fc07b9e89f5be06b713071d4',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
  });
  it('can .skip2 a migration and replace it (back-fit bugfix) multiple times without re-executing', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
      },
    });
    const payload = { panda: 'Wally' };
    const version = undefined;

    const { nextPayload, nextVersion } = await migrate({
      payload,
      config: {
        ...config,
        version,
      },
    });
    expect(nextPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
    });
    expect(nextVersion).toBe(
      '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    //now we realised there's a bug in add-panda.js (it overwrites pre-existing pandas)
    //so we replace that script
    await fs.rename(
      `${directory}/add-panda/add-panda.js`,
      `${directory}/add-panda/add-panda.skip.js`,
    );
    await fs.writeFile(
      path.resolve(`${directory}/add-panda/add-panda.js`),
      `export default (payload) => ({
        ...payload,
        panda: payload.panda ? payload.panda + ' Amended 1'  : "Ralph"
      });`,
      {
        encoding: 'utf8',
      },
    );

    //we want that script not to re-execute
    const { nextPayload: followOnPayload, nextVersion: followOnVersion } =
      await migrate({
        payload: nextPayload,
        config: {
          ...config,
          version: nextVersion,
        },
      });
    expect(followOnPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan', //no change because corrected script does not re-execute
    });
    expect(followOnVersion).toBe(
      'a5a552f9e6211e2262f9e36810b03473f9ea77a10f4811b4d78eb667e923dac6',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    //skip a file a second time
    await fs.rename(
      `${directory}/add-panda/add-panda.js`,
      `${directory}/add-panda/add-panda.skip2.js`,
    );
    await fs.writeFile(
      path.resolve(`${directory}/add-panda/add-panda.js`),
      `export default (payload) => ({
        ...payload,
        panda: payload.panda ? payload.panda + ' Amended 2'  : "Ralph"
      });`,
      {
        encoding: 'utf8',
      },
    );

    //we want that script not to re-execute
    const { nextPayload: finalPayload, nextVersion: finalVersion } =
      await migrate({
        payload: followOnPayload,
        config: {
          ...config,
          version: followOnVersion,
        },
      });
    expect(finalPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan', //no change because corrected script does not re-execute
    });
    expect(finalVersion).toBe(
      'd8772f405d0a93ea389885e2802a47b004c4b9c32e09da0626a7673c76d6700b',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    //final test that the new script executes on an old payload with no version
    const { nextPayload: oldUpgradedPayload, nextVersion: oldUpgradedVersion } =
      await migrate({
        payload: { panda: 'Wally' },
        config: {
          ...config,
        },
      });
    expect(oldUpgradedPayload).toEqual({
      panda: 'Wally Amended 2',
      zebra: 'Lachlan',
    });
    expect(oldUpgradedVersion).toBe(
      'd8772f405d0a93ea389885e2802a47b004c4b9c32e09da0626a7673c76d6700b',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    const migrationsHistory = await database.any(
      'SELECT * from animals_migrations',
    );
    //the hash in the database should reflect the latest file hash
    expect(migrationsHistory[0].file_hash).toBe(
      hash(
        `export default (payload) => ({
        ...payload,
        panda: payload.panda ? payload.panda + ' Amended 2'  : "Ralph"
      });`,
      ),
    );
  });
  it('can backfit a script earlier in the sequence and have it execute', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
      },
    });
    const payload = { panda: 'Wally' };
    const version = undefined;

    const { nextPayload, nextVersion } = await migrate({
      payload,
      config: {
        ...config,
        version,
      },
    });
    expect(nextPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
    });
    expect(nextVersion).toBe(
      '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    //now we backfit a cleanup script earlier in the sequence
    await addMigrationFile(
      'cleanup-script',
      'export default (payload) => ({...payload, ocelot: "Elliot"});',
      '1.1',
    );

    //we want backfitted script to execute on the existing payload
    const { nextPayload: finalPayload, nextVersion: finalVersion } =
      await migrate({
        payload: nextPayload,
        config: {
          ...config,
          version: nextVersion,
        },
      });
    expect(finalPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
      ocelot: 'Elliot',
    });
    expect(finalVersion).toBe(
      '7204ce52d1a8d5bc39a6b5c2772c515af86a31f9558c89cc2ea2f194a108d18e',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
  });
  it('can recover from a merge conflicts via manual resolution', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
      },
    });
    const payload = {};
    const version = undefined;

    const { nextPayload, nextVersion } = await migrate({
      payload,
      config: {
        ...config,
        version,
      },
    });
    expect(nextPayload).toEqual({
      panda: 'Ralph',
    });
    expect(nextVersion).toBe(
      'c9005453332d336b8667ca748c5c6e536ff16cb6568711e94d0c0e07c97d0088',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    await addMigrationFile(
      'add-zebra',
      'export default (payload) => ({...payload, zebra: "Lachlan"});',
      '2',
    );

    const { nextPayload: interimPayload, nextVersion: interimVersion } =
      await migrate({
        payload: nextPayload,
        config: {
          ...config,
          version: nextVersion,
        },
      });
    expect(interimPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
    });
    expect(interimVersion).toBe(
      '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    await addMigrationFile(
      'add-ocelot',
      'export default (payload) => ({...payload, ocelot: "Elliot"});',
      '2',
    );
    const conflictedPlan = `[
      {
        "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
        "fileName": "add-panda.js",
        "sequence": "1"
      },
      {
      <<<<<<< HEAD
        "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
        "fileName": "add-zebra.js",
      =======
        "fileHash": "e993b8c0129650efb8644f1c207c084ff960c0de04c2e88c81ea057865fe4604",
        "fileName": "add-ocelot.js",
      >>>>>>> master
        "sequence": "2"
      }
    ]`;
    await fs.writeFile(path.resolve(`${directory}/plan.json`), conflictedPlan, {
      encoding: 'utf8',
    });
    await expect(
      migrate({
        payload: nextPayload,
        config: {
          ...config,
          version: nextVersion,
        },
      }),
    ).rejects.toThrow(
      'Invalid JSON for migrator plan.js (unable to parse file)',
    );

    const resolvedPlan = `[
      {
        "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
        "fileName": "add-panda.js",
        "sequence": "1"
      },
      {
        "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
        "fileName": "add-zebra.js",
        "sequence": "2"
      },
      {
        "fileHash": "e993b8c0129650efb8644f1c207c084ff960c0de04c2e88c81ea057865fe4604",
        "fileName": "add-ocelot.js",
        "sequence": "3"
      }
    ]`;
    await fs.writeFile(path.resolve(`${directory}/plan.json`), resolvedPlan, {
      encoding: 'utf8',
    });

    const { nextPayload: finalPayload, nextVersion: finalVersion } =
      await migrate({
        payload: nextPayload,
        config: {
          ...config,
          version: nextVersion,
        },
      });
    expect(finalPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
      ocelot: 'Elliot',
    });
    expect(finalVersion).toBe(
      'af5462b1265365937b14dab1be052e73d1bf218b71a21bf5daebee5a273c1f76',
    );
    expect(consoleWarnSpy).toHaveBeenCalledTimes(1);
    expect(consoleErrorSpy).not.toHaveBeenCalled();
  });
  it('can switch branches with different execution plans', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            },
            {
              "fileHash": "e993b8c0129650efb8644f1c207c084ff960c0de04c2e88c81ea057865fe4604",
              "fileName": "add-ocelot.js",
              "sequence": "3"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
        'add-ocelot': {
          'add-ocelot.js':
            'export default (payload) => ({...payload, ocelot: "Elliot"});',
        },
      },
    });
    const payload = {};
    const version = undefined;

    const { nextPayload, nextVersion } = await migrate({
      payload,
      config: {
        ...config,
        version,
      },
    });
    expect(nextPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
      ocelot: 'Elliot',
    });
    expect(nextVersion).toBe(
      'af5462b1265365937b14dab1be052e73d1bf218b71a21bf5daebee5a273c1f76',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    //simulate checking out a new branch
    await fs.writeFile(
      path.resolve(`${directory}/plan.json`),
      `[
        {
          "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
          "fileName": "add-panda.js",
          "sequence": "1"
        },
        {
          "fileHash": "804f38c5ee2da0e851fc22c58ae964cc2d162783317bb3a418f30f3aa9581ff5",
          "fileName": "add-parrot.js",
          "sequence": "2"
        }
       ]`,
      {
        encoding: 'utf8',
      },
    );
    await fs.unlink(`${directory}/add-zebra/add-zebra.js`);
    await fs.mkdir(`${directory}/add-parrot`, { recursive: true });
    await fs.writeFile(
      path.resolve(`${directory}/add-parrot/add-parrot.js`),
      `export default (payload) => ({...payload, parrot: "Gavin"});`,
      {
        encoding: 'utf8',
      },
    );

    //running the migrator on a payload that was previously migrated on the other feature branch
    const { nextPayload: finalPayload, nextVersion: finalVersion } =
      await migrate({
        payload: nextPayload,
        config: {
          ...config,
          version: nextVersion,
        },
      });
    expect(finalPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan', //previously migrated payloads on this branch will have the artefacts from the other branch
      ocelot: 'Elliot',
      parrot: 'Gavin',
    });
    //version hash should stay the same - since we have not structurally downgraded the dataset
    expect(finalVersion).toBe(
      'af5462b1265365937b14dab1be052e73d1bf218b71a21bf5daebee5a273c1f76',
    );
    expect(consoleWarnSpy).toHaveBeenCalledTimes(3);
    expect(consoleWarnSpy).toHaveBeenCalledWith(
      expect.stringContaining('add-zebra.js'),
    );
    expect(consoleWarnSpy).toHaveBeenCalledWith(
      expect.stringContaining('add-ocelot.js'),
    );
    expect(consoleWarnSpy).toHaveBeenCalledWith(
      expect.stringContaining(
        'Previously executed migration files have been deleted from plan.json (rename file to .skip.js and replace with a new file instead)',
      ),
    );

    //running the migrator on a payload that was previously migrated on the other feature branch
    const {
      nextPayload: finalPayloadFromOld,
      nextVersion: finalVersionFromOld,
    } = await migrate({
      payload,
      config: {
        ...config,
      },
    });
    expect(finalPayloadFromOld).toEqual({
      panda: 'Ralph',
      parrot: 'Gavin',
    });
    expect(finalVersionFromOld).toBe(
      '705b606742c32341bf2193c6c750a650a4f511d0bd779eccab3676f1916119b3',
    );
    expect(consoleWarnSpy).toHaveBeenCalledTimes(3);
    expect(consoleWarnSpy).toHaveBeenCalledWith(
      expect.stringContaining('add-zebra.js'),
    );
    expect(consoleWarnSpy).toHaveBeenCalledWith(
      expect.stringContaining('add-ocelot.js'),
    );
    expect(consoleWarnSpy).toHaveBeenCalledWith(
      expect.stringContaining(
        'Previously executed migration files have been deleted from plan.json (rename file to .skip.js and replace with a new file instead)',
      ),
    );
  });
  it('only executes newer migrations, based on version', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
      },
    });
    const payload = {};
    const version = undefined;

    const { nextPayload, nextVersion } = await migrate({
      payload,
      config: {
        ...config,
        version,
      },
    });
    expect(nextPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
    });
    expect(nextVersion).toBe(
      '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161',
    );
    expect(pipeSpy).toHaveBeenCalledTimes(1);
    const pipedMigrators = pipeSpy.mock.calls[0][0];
    expect(pipedMigrators.length).toBe(2);
    expect(pipedMigrators[0].migrator({})).toEqual({
      panda: 'Ralph',
    });
    expect(pipedMigrators[1].migrator({})).toEqual({
      zebra: 'Lachlan',
    });
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    await addMigrationFile(
      'add-ocelot',
      'export default (payload) => ({...payload, ocelot: "Elliot"});',
      '3',
    );

    const { nextPayload: finalPayload, nextVersion: finalVersion } =
      await migrate({
        payload: nextPayload,
        config: {
          ...config,
          version: nextVersion,
        },
      });
    expect(finalPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
      ocelot: 'Elliot',
    });
    expect(finalVersion).toBe(
      'af5462b1265365937b14dab1be052e73d1bf218b71a21bf5daebee5a273c1f76',
    );
    expect(pipeSpy).toHaveBeenCalledTimes(2);
    const finalPipedMigrators = pipeSpy.mock.calls[1][0];
    expect(finalPipedMigrators.length).toBe(1);
    expect(finalPipedMigrators[0].migrator({})).toEqual({
      ocelot: 'Elliot',
    });
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
  });
  it('never downgrades versions', async () => {
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
      },
    });
    const payload = {};
    const version = undefined;

    const originalPlan = await readPlan(directory);
    const originalVersion = hash(originalPlan);
    expect(originalVersion).toBe(
      '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161',
    );

    const { nextPayload, nextVersion } = await migrate({
      payload,
      config: {
        ...config,
        version,
      },
    });
    expect(nextPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
    });
    expect(nextVersion).toBe(
      '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161',
    );
    expect(pipeSpy).toHaveBeenCalledTimes(1);
    const pipedMigrators = pipeSpy.mock.calls[0][0];
    expect(pipedMigrators.length).toBe(2);
    expect(pipedMigrators[0].migrator({})).toEqual({
      panda: 'Ralph',
    });
    expect(pipedMigrators[1].migrator({})).toEqual({
      zebra: 'Lachlan',
    });
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    await deleteMigrationFile('add-zebra');
    const finalPlan = await readPlan(directory);
    const finalVersionFromPlan = hash(finalPlan);
    const { nextVersion: finalVersion } = await migrate({
      payload: nextPayload,
      config: {
        ...config,
        version: nextVersion,
      },
    });
    expect(finalVersionFromPlan).toBe(
      'c9005453332d336b8667ca748c5c6e536ff16cb6568711e94d0c0e07c97d0088',
    );
    expect(finalVersion).toBe(
      '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161',
    );
  });
  it('does not re-execute when .skipping and then rolling back (throws)', async () => {
    const originalPlan = `[
      {
        "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
        "fileName": "add-panda.js",
        "sequence": "1"
      },
      {
        "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
        "fileName": "add-zebra.js",
        "sequence": "2"
      }
    ]`;
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': originalPlan,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
      },
    });
    const payload = { panda: 'Wally' };
    const version = undefined;

    const { nextPayload, nextVersion } = await migrate({
      payload,
      config: {
        ...config,
        version,
      },
    });
    expect(nextPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan',
    });
    expect(nextVersion).toBe(
      '313c8d96e13d8a4c9e655dca6fda8e2e7ad707f9402532e641493417cb8c6161',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    //now we realised there's a bug in add-panda.js (it overwrites pre-existing pandas)
    //so we replace that script
    await fs.rename(
      `${directory}/add-panda/add-panda.js`,
      `${directory}/add-panda/add-panda.skip.js`,
    );
    await fs.writeFile(
      path.resolve(`${directory}/add-panda/add-panda.js`),
      `export default (payload) => ({
        ...payload,
        panda: payload.panda ? payload.panda + ' Amended 1'  : "Ralph"
      });`,
      {
        encoding: 'utf8',
      },
    );

    //we want that script not to re-execute
    const { nextPayload: followOnPayload, nextVersion: followOnVersion } =
      await migrate({
        payload: nextPayload,
        config: {
          ...config,
          version: nextVersion,
        },
      });
    expect(followOnPayload).toEqual({
      panda: 'Ralph',
      zebra: 'Lachlan', //no change because corrected script does not re-execute
    });
    expect(followOnVersion).toBe(
      'a5a552f9e6211e2262f9e36810b03473f9ea77a10f4811b4d78eb667e923dac6',
    );
    expect(consoleWarnSpy).not.toHaveBeenCalled();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    //roll back to branch before skip fix by removing the skip file and replacing original plan.json
    await fs.unlink(path.resolve(`${directory}/add-panda/add-panda.skip.js`));
    await fs.writeFile(path.resolve(`${directory}/plan.json`), originalPlan, {
      encoding: 'utf8',
    });

    //check that script does not re-execute
    await expect(
      migrate({
        payload: followOnPayload,
        config: {
          ...config,
          version: followOnVersion,
        },
      }),
    ).rejects.toThrow(
      'Not allowed to change migration files (for unreleased local work, you can use the `force` option)',
    );
    expect(followOnVersion).toBe(
      'a5a552f9e6211e2262f9e36810b03473f9ea77a10f4811b4d78eb667e923dac6',
    );
  });
});
