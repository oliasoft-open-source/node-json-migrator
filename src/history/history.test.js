import { getTestDatabase } from '../../test/test-database';
import { printVersionHistory } from './history';

describe('History', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  describe('printVersionHistory', () => {
    it('prints the history of plan.json, for debugging', async () => {
      const consoleLogSpy = jest.spyOn(console, 'log');
      const entity = 'animals';
      const database = await getTestDatabase();
      await database.any(`
        INSERT INTO animals_versions (version, plan)
        VALUES (
          'a64212a5719ad2e0e95805775821922b66e5d0759478fccf642dc8afa4cd7470',
          '[{"fileHash": "0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2", "fileName": "add-panda.js", "sequence": "1"}]'
        )
      `);
      await printVersionHistory({
        config: {
          entity,
          directory: 'test/__testdata__/git',
          database,
        },
      });
      expect(consoleLogSpy).toHaveBeenCalledTimes(1);
      expect(consoleLogSpy).toHaveBeenCalledWith(
        expect.stringContaining('add-ocelot.js'),
      );
    });
    it('does not fail when history contains bad plan', async () => {
      const consoleLogSpy = jest.spyOn(console, 'log');
      const consoleErrorSpy = jest.spyOn(console, 'error');
      const entity = 'animals';
      const database = await getTestDatabase();
      await database.any(`
        INSERT INTO animals_versions (version, plan)
        VALUES (
          'a64212a5719ad2e0e95805775821922b66e5d0759478fccf642dc8afa4cd7470',
          '{]TerribleJSON'
        )
      `);
      await printVersionHistory({
        config: {
          entity,
          directory: 'test/__testdata__/git',
          database,
        },
      });
      expect(consoleLogSpy).toHaveBeenCalledTimes(1);
      expect(consoleLogSpy).toHaveBeenCalledWith(
        expect.stringContaining('add-ocelot.js'),
      );
      expect(consoleErrorSpy).toHaveBeenCalledTimes(1);
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringContaining('Unable to parse plan.json'),
      );
    });
    it('logs error upon failure', async () => {
      const consoleLogSpy = jest.spyOn(console, 'log');
      const consoleErrorSpy = jest.spyOn(console, 'error');
      const entity = 'animals';
      await expect(
        printVersionHistory({
          config: {
            entity,
            directory: 'test/__testdata__/git',
            database: 'BadDataBaseObject',
          },
        }),
      ).rejects.toThrow('Unable to print debug history');
      expect(consoleLogSpy).not.toHaveBeenCalled();
      expect(consoleErrorSpy).toHaveBeenCalledTimes(2);
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringContaining('TypeError: db.any is not a function'),
      );
    });
  });
});
