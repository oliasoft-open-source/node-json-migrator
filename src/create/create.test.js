import path from 'path';
import { promises as fs } from 'fs';
import mockFs from 'mock-fs';
import { createMigration } from './create';

const fsMockModules = {
  node_modules: mockFs.load(path.resolve(__dirname, '../../node_modules')),
};

describe('createMigration behavioural test', () => {
  afterEach(() => {
    mockFs.restore();
    jest.clearAllMocks();
  });
  it('creates a new migration file', async () => {
    const directory = 'path/to/migrations';
    mockFs({
      ...fsMockModules,
      [directory]: {
        'plan.json': `[
            {
              "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
              "fileName": "add-panda.js",
              "sequence": "1"
            },
            {
              "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
              "fileName": "add-zebra.js",
              "sequence": "2"
            }
          ]`,
        'add-panda': {
          'add-panda.js':
            'export default (payload) => ({...payload, panda: "Ralph"});',
        },
        'add-zebra': {
          'add-zebra.js':
            'export default (payload) => ({...payload, zebra: "Lachlan"});',
        },
      },
    });
    await createMigration(directory, 'test-creating-migration-file');
    const createdMigrationFile = await fs.readFile(
      `${directory}/test-creating-migration-file/test-creating-migration-file.js`,
      { encoding: 'utf8' },
    );
    const createdTestFile = await fs.readFile(
      `${directory}/test-creating-migration-file/test-creating-migration-file.test.js`,
      { encoding: 'utf8' },
    );
    const updatedPlanFile = await fs.readFile(`${directory}/plan.json`, {
      encoding: 'utf8',
    });
    expect(createdMigrationFile.includes('produce(dataset, (draft) => {')).toBe(
      true,
    );
    expect(
      createdTestFile.includes('const nextDataset = migrate(dataset);'),
    ).toBe(true);
    expect(updatedPlanFile).toMatchStringIgnoringFormatting(`[
      {
        "fileHash": "4ce1a0a766a712db7b2516947b38822178a1df5128d642e0b3adce37461cfe6c",
        "fileName": "add-panda.js",
        "sequence": "1"
      },
      {
        "fileHash": "bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e",
        "fileName": "add-zebra.js",
        "sequence": "2"
      },
      {
        "fileHash": "",
        "fileName": "test-creating-migration-file.js",
        "sequence": "3"
      }
    ]`);
  });
  it('throws error when description is invalid', async () => {
    const directory = 'path/to/migrations';
    mockFs({
      ...fsMockModules,
      [directory]: {},
    });
    await expect(
      createMigration(directory, '-test-creatingHorribleMigration-file'),
    ).rejects.toThrow('Invalid migration description');
  });
  it('throws error when unable to write file', async () => {
    jest.spyOn(fs, 'writeFile').mockImplementation(() => {
      throw new Error('Something wrong with writing files');
    });
    const directory = 'non-existent-path/to/migrations';
    mockFs({
      ...fsMockModules,
      'path/to/migrations': {},
    });
    await expect(
      createMigration(directory, 'test-creating-migration-file'),
    ).rejects.toThrow('Unable to create migration file');
  });
});
