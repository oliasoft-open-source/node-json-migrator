import {
  pgpHelpers,
  getEmptyTestDatabase,
  getTestDatabase,
} from '../../test/test-database';
import {
  createMigrationsTables,
  getPlanFromVersion,
  insertMigrationsRecords,
  replaceMigrationsRecords,
  getMigrationRecords,
  insertVersions,
  getVersions,
} from './database';

describe('DB', () => {
  it('createMigrationsTables creates the migrations tables', async () => {
    const entityName = 'animals';
    const database = await getEmptyTestDatabase();
    await createMigrationsTables(database, entityName);
    const migrations = await database.one(
      'SELECT COUNT(*) FROM animals_migrations',
    );
    const versions = await database.one(
      'SELECT COUNT(*) FROM animals_versions',
    );
    expect(migrations.count).toBe(0);
    expect(versions.count).toBe(0);
  });
  describe('getPlanFromVersion', () => {
    it('returns null when the version does not exist', async () => {
      const entityName = 'animals';
      const database = await getTestDatabase();
      const plan = await getPlanFromVersion(
        database,
        entityName,
        'non-existent-version',
      );
      expect(plan).toBe(null);
    });
    it('gets the historical plan from a version (hashtable lookup)', async () => {
      const database = await getTestDatabase();
      await database.any(`
        INSERT INTO animals_versions (version, plan)
        VALUES (
          'a64212a5719ad2e0e95805775821922b66e5d0759478fccf642dc8afa4cd7470',
          '[{"fileHash": "0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2", "fileName": "add-panda.js"}]'
        )
      `);
      const indexFile = await getPlanFromVersion(
        database,
        'animals',
        'a64212a5719ad2e0e95805775821922b66e5d0759478fccf642dc8afa4cd7470',
      );
      expect(typeof indexFile).toBe('string');
      expect(JSON.parse(indexFile)[0].fileName).toBe('add-panda.js');
    });
  });
  describe('insertVersions', () => {
    it('inserts the plan for a version into the hashtable', async () => {
      const database = await getTestDatabase();
      const version =
        'd54d31678f4dd7118c5b6fb71ccb01f4e46405869d2e9f93539b0886595037a2';
      const plan =
        '[{"file_hash": "0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2", "file_name": "add-panda.js", "sequence": "1"}]';
      const records = [{ version, plan }];
      await insertVersions(database, pgpHelpers, 'animals', records);
      const versions = await database.many('SELECT * FROM animals_versions');
      expect(versions.length).toBe(1);
      expect(versions[0].version).toBe(version);
    });
    it('inserts the plans for multiple versions into the hashtable and does nothing on conflict', async () => {
      const database = await getTestDatabase();
      const records = [
        {
          version:
            'd54d31678f4dd7118c5b6fb71ccb01f4e46405869d2e9f93539b0886595037a2',
          plan: '[{"file_hash": "0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2", "file_name": "add-panda.js", "sequence": "1"}]',
        },
        {
          version:
            'd54d31678f4dd7118c5b6fb71ccb01f4e46405869d2e9f93539b0886595037a2',
          plan: '[{"file_hash": "0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2", "file_name": "add-panda.js", "sequence": "1"}]',
        },
        {
          version:
            'fdb2d364cba3f2acd252ba6f1245678ccd893c6ec1a23ff3710493caf85ea891',
          plan: `[
            {"file_hash": "0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2", "file_name": "add-panda.js", "sequence": "1"},
            {"file_hash": "1810f754db417cf9a0206fcb9a93048b6628bd9e634c271d988367f2016314b1", "file_name": "add-zebra.js", "sequence": "2"}
          ]`,
        },
      ];
      await insertVersions(database, pgpHelpers, 'animals', records);
      const versions = await database.many('SELECT * FROM animals_versions');
      expect(versions.length).toBe(2);
      expect(versions[1].version).toBe(
        'fdb2d364cba3f2acd252ba6f1245678ccd893c6ec1a23ff3710493caf85ea891',
      );
    });
    it('does not fail when invoked with a pre-existing version', async () => {
      const database = await getTestDatabase();
      const version =
        'a64212a5719ad2e0e95805775821922b66e5d0759478fccf642dc8afa4cd7470';
      const plan =
        '[{"file_hash": "0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2", "file_name": "add-panda.js"}]';
      const records = [{ version, plan }];
      await insertVersions(database, pgpHelpers, 'animals', records);
      await insertVersions(database, pgpHelpers, 'animals', records);
      const versions = await database.many('SELECT * FROM animals_versions');
      expect(versions.length).toBe(1);
      expect(versions[0].version).toBe(version);
    });
  });
  describe('insertMigrationRecords', () => {
    it('inserts the migrations into the entity_migrations table', async () => {
      const database = await getTestDatabase();
      const migrations = [
        {
          fileHash:
            '0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2',
          fileName: 'add-panda.js',
          sequence: '1',
        },
        {
          fileHash:
            'bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e',
          fileName: 'add-zebra.js',
          sequence: '2',
        },
      ];
      await insertMigrationsRecords(
        database,
        pgpHelpers,
        'animals',
        migrations,
      );
      const migrationRecords = (
        await database.many('SELECT * FROM animals_migrations')
      ).map((m) => ({
        fileHash: m.file_hash,
        fileName: m.file_name,
        sequence: m.sequence,
      }));
      expect(migrationRecords.length).toBe(2);
      expect(migrationRecords).toStrictEqual(migrations);
    });
    it('throws when there are file hash conflicts', async () => {
      const database = await getTestDatabase();
      await createMigrationsTables(database, 'animals');
      const migrations = [
        {
          fileHash:
            '0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2',
          fileName: 'add-panda.js',
          sequence: '1',
        },
        {
          fileHash:
            '0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2',
          fileName: 'add-zebra.js',
          sequence: '2',
        },
      ];
      await expect(
        insertMigrationsRecords(database, pgpHelpers, 'animals', migrations),
      ).rejects.toThrow('already exists');
    });
    it('throws when there are sequence conflicts', async () => {
      const database = await getTestDatabase();
      await createMigrationsTables(database, 'animals');
      const migrations = [
        {
          fileHash:
            '0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2',
          fileName: 'add-panda.js',
          sequence: '1.2.3',
        },
        {
          fileHash:
            'bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e',
          fileName: 'add-zebra.js',
          sequence: '1.2.3',
        },
      ];
      await expect(
        insertMigrationsRecords(database, pgpHelpers, 'animals', migrations),
      ).rejects.toThrow('already exists');
    });
  });
  describe('replaceMigrationsRecords', () => {
    it('replaces the migrations into the entity_migrations table', async () => {
      const database = await getTestDatabase();
      await database.any(`
        INSERT INTO animals_migrations (file_hash, file_name, sequence)
        VALUES 
        ('bcd950c91ab7217819ad949321665abc290f1ad40966cd2092d131700daee8df', 'add-panda.js', '1'),
        ('5d65d6713b02c30e737b81e663d728d9614d4da22248047422544a5545e3fdbe', 'add-zebra.js', '2'),
        ('aae68d9fd25a8b2d01cb1d18d93f8bf76929767075af2829636dba1016b9208e', 'add-ocelot.js', '3')
      `);
      const migrations = [
        {
          fileHash:
            '0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2',
          fileName: 'add-panda.js',
          sequence: '1',
        },
        {
          fileHash:
            'bffcddc2b56e6048de74037bcb65bfffd95c3376c139126a74c06afcb9866d0e',
          fileName: 'add-zebra.js',
          sequence: '2',
        },
        {
          fileHash:
            'c7c6227421ed47e94142228ed9b1a17e4d877c211b9c599339d15ead9bac389f',
          fileName: 'add-aardvark.js',
          sequence: '3',
        },
      ];
      await replaceMigrationsRecords(
        database,
        pgpHelpers,
        'animals',
        migrations,
      );
      const migrationRecords = (
        await database.many('SELECT * FROM animals_migrations')
      ).map((m) => ({
        fileHash: m.file_hash,
        fileName: m.file_name,
        sequence: m.sequence,
      }));
      expect(migrationRecords.length).toBe(3);
      expect(migrationRecords).toStrictEqual(migrations);
    });
  });
  describe('getMigrations', () => {
    it('gets the migrations from the entity_migrations table', async () => {
      const database = await getTestDatabase();
      await database.any(`
        INSERT INTO animals_migrations (file_hash, file_name, sequence)
        VALUES ('0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2', 'add-panda.js', '1')
      `);
      const migrationRecords = await getMigrationRecords(database, 'animals');
      expect(migrationRecords.length).toBe(1);
      expect(migrationRecords).toStrictEqual([
        {
          fileHash:
            '0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2',
          fileName: 'add-panda.js',
          sequence: '1',
        },
      ]);
    });
  });
  describe('getVersions', () => {
    it('gets the versions from the versions table', async () => {
      const database = await getTestDatabase();
      await database.any(`
        INSERT INTO animals_versions (version, plan)
        VALUES
        ('3b9d7a5efdf8d931cec142a245083f16ad8b2dd73c51efe85dc21b7fa985b33c', 'monkeys'),
        ('af5964fcd765fce50ef10c0be6d397c11ab12e45dcdaca2e9d1a47fb32b92678', 'bananas')
      `);
      const versions = await getVersions(database, 'animals');
      expect(versions.length).toBe(2);
      expect(versions[1].version).toStrictEqual(
        'af5964fcd765fce50ef10c0be6d397c11ab12e45dcdaca2e9d1a47fb32b92678',
      );
    });
  });
});
