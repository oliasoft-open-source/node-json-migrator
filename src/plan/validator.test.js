import {
  validateFileName,
  validateFileDescription,
  throwIfFilesHaveChanged,
  throwIfFileNamesInvalid,
  throwIfSequenceNotUnique,
  throwIfSequenceHasIntegerGaps,
  throwIfFileNamesNotUnique,
  throwIfFilesNotFound,
  validatePlan,
} from './validator';

const consoleErrorSpy = jest.spyOn(console, 'error');

describe('JSON migrator', () => {
  describe('validateFileDescriptionFormat', () => {
    it('checks filename format is valid', async () => {
      expect(validateFileDescription('add')).toBe(true);
      expect(validateFileDescription('add-animals')).toBe(true);
      expect(validateFileDescription('add-more-animals')).toBe(true);
      expect(validateFileDescription('add-three-animals')).toBe(true);
      expect(validateFileDescription('')).toBe(false);
      expect(validateFileDescription(undefined)).toBe(false);
      expect(validateFileDescription(null)).toBe(false);
      expect(validateFileDescription('-add-more-animals')).toBe(false);
      expect(validateFileDescription('add-more-animals-')).toBe(false);
      expect(validateFileDescription('-add-more-animals-')).toBe(false);
      expect(validateFileDescription('addMore-animals')).toBe(false);
      expect(validateFileDescription('add-3-animals')).toBe(false);
      expect(validateFileDescription('add-animals3')).toBe(false);
      expect(validateFileDescription('add_animals')).toBe(false);
    });
  });
  describe('validateFileNameFormat', () => {
    it('checks filename format is valid', async () => {
      expect(validateFileName('add.js')).toBe(true);
      expect(validateFileName('add-animals.js')).toBe(true);
      expect(validateFileName('add-animals.skip.js')).toBe(true);
      expect(validateFileName('add-rare-animals.js')).toBe(true);
      expect(validateFileName('-add-animals.js')).toBe(false);
      expect(validateFileName('add-animals-.js')).toBe(false);
      expect(validateFileName('-add-animals-.js')).toBe(false);
      expect(validateFileName('add_animals.js')).toBe(false);
      expect(validateFileName('addAnimals.js')).toBe(false);
      expect(validateFileName('add-animals.sql')).toBe(false);
      expect(validateFileName('foo-13-add-animals-2021-10-09.skip.js')).toBe(
        false,
      );
      expect(validateFileName('add-animals-2021-10-09.skip.js')).toBe(false);
    });
  });
  describe('throwIfFileNamesInvalid', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    it('does not throw when filenames are valid', async () => {
      const consoleSpy = jest
        .spyOn(console, 'error')
        .mockImplementation(() => {});
      const fileEntries = [
        {
          fileName: 'add-panda.js',
          isValidFileName: true,
        },
      ];
      expect(() => {
        throwIfFileNamesInvalid(fileEntries);
      }).not.toThrow();
      expect(consoleSpy).not.toHaveBeenCalled();
    });
    it('throws when filenames are valid', async () => {
      const consoleSpy = jest
        .spyOn(console, 'error')
        .mockImplementation(() => {});
      const fileEntries = [
        {
          fileName: 'foobar.js',
          isValidFileName: false,
        },
      ];
      expect(() => {
        throwIfFileNamesInvalid(fileEntries);
      }).toThrow('Invalid migration filename format (use kebab-case)');
      expect(consoleSpy).toHaveBeenCalledWith(
        expect.stringMatching('foobar.js'),
      );
      expect(consoleSpy).toHaveBeenCalledTimes(1);
    });
  });
  describe('throwIfFilesHaveChanged', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    it('does not throw if file entries, plan, and history are the same', async () => {
      const consoleSpy = jest
        .spyOn(console, 'error')
        .mockImplementation(() => {});
      const migrationEntries = [
        {
          fileName: 'refactor-monkeys.js',
          fileHash: 'X',
          fileHashFromPlan: 'X',
          fileHashFromHistory: 'X',
        },
        {
          fileName: 'refactor-zebras.js',
          fileHash: 'Y',
          fileHashFromPlan: 'Y',
          fileHashFromHistory: 'Y',
        },
      ];
      expect(() => {
        throwIfFilesHaveChanged(migrationEntries);
      }).not.toThrow();
      expect(consoleSpy).not.toHaveBeenCalled();
    });
    it('does not throw if there is no history (first run)', async () => {
      const consoleSpy = jest
        .spyOn(console, 'error')
        .mockImplementation(() => {});
      const migrationEntries = [
        {
          fileName: 'refactor-monkeys.js',
          fileHash: 'X',
          fileHashFromPlan: 'X',
        },
        {
          fileName: 'refactor-zebras.js',
          fileHash: 'Y',
          fileHashFromPlan: 'Y',
        },
      ];
      expect(() => {
        throwIfFilesHaveChanged(migrationEntries);
      }).not.toThrow();
      expect(consoleSpy).not.toHaveBeenCalled();
    });
    it('does not throw if new scripts have unspecified file hashes', async () => {
      const consoleSpy = jest
        .spyOn(console, 'error')
        .mockImplementation(() => {});
      const migrationEntries = [
        {
          fileName: 'refactor-monkeys.js',
          fileHash: 'X',
        },
        {
          fileName: 'refactor-zebras.js',
          fileHash: 'Y',
          fileHashFromPlan: '',
        },
      ];
      expect(() => {
        throwIfFilesHaveChanged(migrationEntries);
      }).not.toThrow();
      expect(consoleSpy).not.toHaveBeenCalled();
    });
    it('throws when an existing file(s) have changed', () => {
      const consoleSpy = jest
        .spyOn(console, 'error')
        .mockImplementation(() => {});
      const migrationEntries = [
        {
          fileName: 'refactor-monkeys.js',
          fileHash: 'X2',
          fileHashFromPlan: 'X',
          fileHashFromHistory: 'X',
        },
        {
          fileName: 'refactor-zebras.js',
          fileHash: 'Y',
          fileHashFromPlan: 'Y2',
          fileHashFromHistory: 'Y',
        },
        {
          fileName: 'refactor-donkeys.js',
          fileHash: 'Z',
          fileHashFromPlan: 'Z',
          fileHashFromHistory: 'Z2',
        },
      ];
      expect(() => {
        throwIfFilesHaveChanged(migrationEntries);
      }).toThrow(
        'Not allowed to change migration files (for unreleased local work, you can use the `force` option)',
      );
      expect(consoleSpy).toHaveBeenCalledTimes(3);
      expect(consoleSpy).toHaveBeenCalledWith(
        expect.stringMatching('refactor-monkeys.js'),
      );
      expect(consoleSpy).toHaveBeenCalledWith(
        expect.stringMatching('refactor-zebras.js'),
      );
      expect(consoleSpy).toHaveBeenCalledWith(
        expect.stringMatching('refactor-donkeys.js'),
      );
    });
    it('does not throw when an existing file(s) have changed by skipping', () => {
      const consoleSpy = jest
        .spyOn(console, 'error')
        .mockImplementation(() => {});
      const migrationEntries = [
        {
          fileName: 'refactor-monkeys.js',
          fileHash: 'X',
          fileHashFromPlan: 'X',
          fileHashFromHistory: 'X',
        },
        {
          fileName: 'refactor-zebras.js',
          fileHash: 'Y2',
          fileHashFromPlan: 'Y',
          fileHashFromHistory: 'Y',
          skippedFileHashes: ['Y'],
        },
        {
          fileName: 'refactor-donkeys.js',
          fileHash: 'Z',
          fileHashFromPlan: 'Z',
          fileHashFromHistory: 'Z',
        },
      ];
      expect(() => {
        throwIfFilesHaveChanged(migrationEntries);
      }).not.toThrow();
      expect(consoleSpy).not.toHaveBeenCalled();
    });
  });
  describe('throwIfSequenceNotUnique', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    it('does not throw if file entries have unique sequence numbers', async () => {
      const fileEntries = [
        {
          fileName: 'refactor-monkeys.skip.js',
          sequence: '1',
        },
        {
          fileName: 'refactor-monkeys.js',
          sequence: '1.1',
        },
        {
          fileName: 'refactor-baboons.js',
          sequence: '2',
        },
      ];
      expect(() => {
        throwIfSequenceNotUnique(fileEntries);
      }).not.toThrow();
      expect(consoleErrorSpy).not.toHaveBeenCalled();
    });
    it('throws if file entries have duplicated sequence numbers', async () => {
      const fileEntries = [
        {
          fileName: 'refactor-monkeys.js',
          sequence: '1',
        },
        {
          fileName: 'refactor-baboons.js',
          sequence: '1',
        },
        {
          fileName: 'refactor-fixed-baboons.js',
          sequence: '1.1',
        },
        {
          fileName: 'refactor-aardvarks.js',
          sequence: '2',
        },
        {
          fileName: 'refactor-rhinos.js',
          sequence: '1',
        },
      ];
      expect(() => {
        throwIfSequenceNotUnique(fileEntries);
      }).toThrow('Migrations must have unique sequence numbers in plan.json');
      expect(consoleErrorSpy).toHaveBeenCalledTimes(3);
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringMatching('refactor-monkeys.js'),
      );
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringMatching('refactor-baboons.js'),
      );
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringMatching('refactor-rhinos.js'),
      );
      expect(consoleErrorSpy).not.toHaveBeenCalledWith(
        expect.stringMatching('refactor-aardvarks.js'),
      );
      expect(consoleErrorSpy).not.toHaveBeenCalledWith(
        expect.stringMatching('refactor-fixed-baboons.js'),
      );
    });
  });
  describe('throwIfSequenceHasIntegerGaps', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    it('does not throw if sequences have no integer (top-level) gaps', () => {
      const fileEntries = [
        {
          sequence: '1',
        },
        {
          sequence: '2.1',
        },
        {
          sequence: '3.2.4',
        },
        {
          sequence: '4',
        },
        {
          sequence: '5',
        },
        {
          sequence: '6',
        },
        {
          sequence: '7',
        },
        {
          sequence: '8',
        },
        {
          sequence: '9',
        },
        {
          sequence: '10',
        },
        {
          sequence: '11',
        },
        {
          sequence: '12',
        },
      ];
      expect(() => {
        throwIfSequenceHasIntegerGaps(fileEntries);
      }).not.toThrow();
      expect(consoleErrorSpy).not.toHaveBeenCalled();
    });
    it('throws if sequences have integer (top-level) gaps', () => {
      const fileEntries = [
        {
          sequence: '1',
        },
        {
          sequence: '1.1',
        },
        {
          sequence: '3',
        },
        {
          sequence: '4',
        },
        {
          sequence: '7',
        },
      ];
      expect(() => {
        throwIfSequenceHasIntegerGaps(fileEntries);
      }).toThrow(
        'Migration sequence numbers in plan.json have unexpected gaps: 2, 5, 6',
      );
      expect(consoleErrorSpy).not.toHaveBeenCalled();
    });
    it('does not care about gaps in subsequences', () => {
      const fileEntries = [
        {
          sequence: '1',
        },
        {
          sequence: '1.2',
        },
        {
          sequence: '2.1.0',
        },
        {
          sequence: '2.1.4',
        },
      ];
      expect(() => {
        throwIfSequenceHasIntegerGaps(fileEntries);
      }).not.toThrow();
      expect(consoleErrorSpy).not.toHaveBeenCalled();
    });
    it('handles unsorted input', () => {
      const fileEntries = [
        {
          sequence: '7',
        },
        {
          sequence: '1',
        },
        {
          sequence: '3',
        },
        {
          sequence: '1.1',
        },
        {
          sequence: '4',
        },
      ];
      expect(() => {
        throwIfSequenceHasIntegerGaps(fileEntries);
      }).toThrow(
        'Migration sequence numbers in plan.json have unexpected gaps: 2, 5, 6',
      );
      expect(consoleErrorSpy).not.toHaveBeenCalled();
    });
  });
  describe('throwIfFileNamesNotUnique', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    it('does not throw if file entries have unique file names', async () => {
      const fileEntries = [
        {
          fileName: 'refactor-monkeys.js',
        },
        {
          fileName: 'refactor-baboons.js',
        },
        {
          fileName: 'refactor-orangutans.js',
        },
      ];
      expect(() => {
        throwIfFileNamesNotUnique(fileEntries);
      }).not.toThrow();
      expect(consoleErrorSpy).not.toHaveBeenCalled();
    });
    it('throws if file entries have non-unique file names', async () => {
      const fileEntries = [
        {
          fileName: 'refactor-monkeys.js',
        },
        {
          fileName: 'refactor-baboons.js',
        },
        {
          fileName: 'refactor-monkeys.js',
        },
      ];
      expect(() => {
        throwIfFileNamesNotUnique(fileEntries);
      }).toThrow('Migration file names must be unique');
      expect(consoleErrorSpy).toHaveBeenCalledTimes(1);
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringMatching('refactor-monkeys.js'),
      );
    });
  });
  describe('throwIfFilesNotFound', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    it('does not throw when there are no migrations', () => {
      const migrationEntries = [];
      expect(() => {
        throwIfFilesNotFound(migrationEntries);
      }).not.toThrow();
      expect(consoleErrorSpy).not.toHaveBeenCalled();
    });
    it('does not throw when all migrations have files', () => {
      const migrationEntries = [
        {
          fileName: 'foo.js',
          script: '() => {}',
          migrator: () => {},
          skippedFileHashes: [],
        },
        {
          fileName: 'bar.js',
          script: '() => {}',
          migrator: () => {},
          skippedFileHashes: [],
        },
      ];
      expect(() => {
        throwIfFilesNotFound(migrationEntries);
      }).not.toThrow();
      expect(consoleErrorSpy).not.toHaveBeenCalled();
    });
    it('throws if any file is not found', () => {
      const migrationEntries = [
        { fileName: 'foo.js', skippedFileHashes: [] },
        {
          fileName: 'bar.js',
          script: '() => {}',
          migrator: () => {},
          skippedFileHashes: [],
        },
      ];
      expect(() => {
        throwIfFilesNotFound(migrationEntries);
      }).toThrow('Migration files from plan.json are missing from filesystem');
      expect(consoleErrorSpy).toHaveBeenCalledTimes(1);
      expect(consoleErrorSpy).toHaveBeenCalledWith(
        expect.stringMatching('foo.js'),
      );
    });
  });
  describe('isValidPlan', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    describe('validates index array according to schema', () => {
      it('returns true for empty array', () => {
        expect(validatePlan([])).toBe(true);
        expect(consoleErrorSpy).not.toHaveBeenCalled();
      });
      it('returns true for valid index', () => {
        expect(
          validatePlan([
            {
              fileHash:
                '0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2',
              fileName: 'add-animals.js',
              sequence: '1',
            },
          ]),
        ).toBe(true);
        expect(consoleErrorSpy).not.toHaveBeenCalled();
      });
      it('returns false for malformed sequence', () => {
        expect(
          validatePlan([
            {
              fileHash:
                '0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2',
              fileName: 'add-animals.js',
              sequence: '1foo345',
            },
          ]),
        ).toBe(false);
        expect(consoleErrorSpy).toHaveBeenCalledWith(
          expect.stringContaining('must match pattern'),
        );
      });
      it('returns false for sequences with leading zeros', () => {
        expect(
          validatePlan([
            {
              fileHash:
                '0d2941355c8b221ec6e6b768bb1d7b5bab889126aba11b48c82030fb2a33ebc2',
              fileName: 'add-animals.js',
              sequence: '1.01',
            },
          ]),
        ).toBe(false);
        expect(consoleErrorSpy).toHaveBeenCalledWith(
          expect.stringContaining('must match pattern'),
        );
      });
      it('returns false for empty string', () => {
        expect(validatePlan('')).toBe(false);
        expect(consoleErrorSpy).toHaveBeenCalledWith('must be array');
      });
      it('returns false for object', () => {
        expect(validatePlan({})).toBe(false);
        expect(consoleErrorSpy).toHaveBeenCalledWith('must be array');
      });
      it('returns false for invalid index', () => {
        expect(
          validatePlan([
            {
              fileRubbish: 'zzz',
              fileName: 'add-animals.js',
            },
          ]),
        ).toBe(false);
        expect(consoleErrorSpy).toHaveBeenCalledWith(
          expect.stringContaining(`must have required property 'fileHash'`),
        );
        expect(consoleErrorSpy).toHaveBeenCalledWith(
          expect.stringContaining(`must have required property 'sequence'`),
        );
        expect(consoleErrorSpy).toHaveBeenCalledWith(
          expect.stringContaining(`must NOT have additional properties`),
        );
      });
    });
  });
});
