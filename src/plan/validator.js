import chalk from 'chalk';
import { isString, xor } from 'lodash';
import Ajv from 'ajv';
import ajvErrors from 'ajv-errors';
import planSchema from './plan.schema.json';

const isNonEmptyString = (input) => isString(input) && input.length > 0;

const ajv = new Ajv({ allErrors: true });
ajvErrors(ajv);

//sequence pattern: https://regex101.com/r/4qHZLm/1
const planValidator = ajv.compile(planSchema);

/**
 * Validates if a plan matches the schema
 *
 * @param {Array<Object>} plan
 * @returns {Boolean} valid
 */

export const validatePlan = (plan) => {
  const valid = planValidator(plan);
  const { errors } = planValidator;
  if (errors?.length) {
    errors.forEach((e) =>
      console.error(
        `${e.message}${e.instancePath ? `at ${e.instancePath}` : ''}`,
      ),
    );
  }
  return valid;
};

/**
 * Validates if file description format is valid
 *
 * @param {String} description
 * @returns {Boolean} valid
 */

export const validateFileDescription = (description) => {
  if (!description) {
    return false;
  }
  //https://regex101.com/r/IO0dzF/1
  const validFileName = /^([a-z]+|(([a-z]+-)+[a-z]+))$/g;
  return validFileName.test(description);
};

/**
 * Validates if fileName format is valid
 *
 * @param {String} fileName
 * @returns {Boolean} valid
 */

export const validateFileName = (fileName) => {
  //https://regex101.com/r/EX2RlS/1
  const validFileName = /^([a-z]+|(([a-z]+-)+[a-z]+))(.js|.skip.js)$/g;
  return validFileName.test(fileName);
};

/**
 * Throws if file names are invalid
 *
 * @param {Array<Object>} migrationEntries
 */

export const throwIfFileNamesInvalid = (fileEntries) => {
  const invalidFileNames = fileEntries
    .filter((f) => !f.isValidFileName)
    .map((f) => f.fileName);
  if (invalidFileNames.length) {
    invalidFileNames.forEach((name) => console.error(chalk.red(name)));
    throw new Error('Invalid migration filename format (use kebab-case)');
  }
};

/**
 * Warns if files have been removed
 *
 * @param {Array<Object>} migrationEntries
 * @param {Array<Object>} historicalMigrations
 */

export const warnIfFilesHaveBeenRemovedFromPlan = (
  migrationEntries,
  historicalMigrations,
) => {
  const fileNames = migrationEntries.map((f) => f.fileName);
  const deletedFilesNames = historicalMigrations
    .filter((e) => !fileNames.includes(e.fileName))
    .map((e) => e.fileName);
  if (deletedFilesNames.length) {
    deletedFilesNames.forEach((name) => console.warn(chalk.yellow(name)));
    console.warn(
      chalk.yellow(
        'Previously executed migration files have been deleted from plan.json (rename file to .skip.js and replace with a new file instead)',
      ),
    );
  }
};

/**
 * Throws error if any pre-existing file has changed
 *
 * @param {Array<Object>} migrationEntries
 */

export const throwIfFilesHaveChanged = (migrationEntries) => {
  const changedFilesNames = migrationEntries
    .filter((m) => {
      const {
        fileHash,
        fileHashFromPlan,
        fileHashFromHistory,
        skippedFileHashes,
      } = m;
      const matchesHistory =
        !isNonEmptyString(fileHashFromHistory) ||
        [fileHash].concat(skippedFileHashes).includes(fileHashFromHistory);
      const matchesPlan =
        !isNonEmptyString(fileHashFromPlan) ||
        [fileHash].concat(skippedFileHashes).includes(fileHashFromPlan);
      return !(matchesHistory && matchesPlan);
    })
    .map((f) => f.fileName);
  if (changedFilesNames.length) {
    changedFilesNames.forEach((name) => console.error(chalk.red(name)));
    throw new Error(
      'Not allowed to change migration files (for unreleased local work, you can use the `force` option)',
    );
  }
};

/**
 * Throws error if previous sequence numbers have changed
 *
 * @param {Array<Object>} migrationEntries
 */

export const throwIfSequenceHasChanged = (migrationEntries) => {
  const changeSequences = migrationEntries
    .filter((m) => {
      const { sequence, sequenceFromHistory } = m;
      const changed =
        isString(sequenceFromHistory) && sequenceFromHistory !== sequence;
      return changed;
    })
    .map((f) => f.fileName);
  if (changeSequences.length) {
    changeSequences.forEach((name) => console.error(chalk.red(name)));
    throw new Error('Not allowed to change migration sequences in plan.json');
  }
};

/**
 * Throws error if sequence numbers are not unique
 *
 * @param {Array<Object>} migrationEntries
 */

export const throwIfSequenceNotUnique = (migrationEntries) => {
  const sequenceNumbers = migrationEntries.map((f) => f.sequence);
  const repeatedSequenceNumbers = sequenceNumbers.filter(
    //https://stackoverflow.com/a/59517965/942635
    (
      (s) => (v) =>
        s.has(v) || !s.add(v)
    )(new Set()),
  );
  const duplicates = migrationEntries
    .filter((f) => repeatedSequenceNumbers.includes(f.sequence))
    .map((f) => f.fileName);
  if (duplicates.length) {
    duplicates.forEach((fileName) => console.error(chalk.red(fileName)));
    throw new Error(
      'Migrations must have unique sequence numbers in plan.json',
    );
  }
};

/**
 * Throws error if sequence numbers are not unique
 *
 * @param {Array<Object>} migrationEntries
 */

export const throwIfSequenceHasIntegerGaps = (migrationEntries) => {
  const toInteger = (sequence) => parseInt(sequence.split('.')[0], 10);
  const unique = (arr) => Array.from(new Set(arr));

  const sequences = migrationEntries.map((s) => s.sequence);
  const sequenceIntegers = sequences.map((s) => toInteger(s));
  const uniqueSequenceIntegers = unique(sequenceIntegers);
  const orderedUniqueSequenceIntegers = uniqueSequenceIntegers.sort(
    (a, b) => a - b,
  );
  const max =
    orderedUniqueSequenceIntegers[orderedUniqueSequenceIntegers.length - 1];
  const expected = Array.from({ length: max }, (v, k) => k + 1);
  const missing = xor(orderedUniqueSequenceIntegers, expected);
  if (missing.length) {
    throw new Error(
      `Migration sequence numbers in plan.json have unexpected gaps: ${missing.join(
        ', ',
      )}`,
    );
  }
};

/**
 * Throws error if file names are not unique
 *
 * @param {Array<Object>} migrationEntries
 */

export const throwIfFileNamesNotUnique = (migrationEntries) => {
  const fileNames = migrationEntries.map((f) => f.fileName);
  const repeatedFileNames = fileNames.filter(
    //https://stackoverflow.com/a/59517965/942635
    (
      (s) => (v) =>
        s.has(v) || !s.add(v)
    )(new Set()),
  );
  if (repeatedFileNames.length) {
    repeatedFileNames.forEach((fileName) => console.error(chalk.red(fileName)));
    throw new Error('Migration file names must be unique');
  }
};

/**
 * Throws error if files listed in plan.json are found
 *
 * @param {Array<Object>} migrationEntries
 * @param {Boolean} importModule
 */

export const throwIfFilesNotFound = (migrationEntries, importModule) => {
  const migrationsWithoutFiles = migrationEntries.filter(
    (m) => !m.script || (importModule && !m.script && !m.migrator),
  );
  if (migrationsWithoutFiles.length) {
    migrationsWithoutFiles.forEach((migration) =>
      console.error(chalk.red(migration.fileName)),
    );
    throw new Error(
      'Migration files from plan.json are missing from filesystem',
    );
  }
};
