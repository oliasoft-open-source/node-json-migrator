import chalk from 'chalk';
import { getHistoricalPlansFromGit, historyError } from '../git/git';
import { getVersions } from '../database/database';

/**
 * Parse plan.json
 *
 * @param {string} plan
 * @returns object|null parsed plan
 */
const parsePlan = (plan) => {
  return (() => {
    try {
      return JSON.parse(plan);
    } catch (e) {
      console.error('Unable to parse plan.json');
      return null;
    }
  })();
};

/**
 * @typedef {Object} Configuration
 * @property {string} directory (path to migrations directory)
 * @property {string} entity (entity name for payload, e.g. dataset)
 * @property {Object} database (connected pg-promise database)
 */

/**
 * Prints the version history of plan.json (for debugging)
 *
 * @param {Object} args
 * @param {Configuration} args.config
 */
export const printVersionHistory = async ({ config }) => {
  const { database, entity, directory } = config;
  try {
    const planFilePath = `${directory}/plan.json`;

    //read version history from database cache
    const versionHistoryFromDatabase = await getVersions(database, entity);
    const fullDatabaseVersionHistory = versionHistoryFromDatabase.map((d) => {
      const plan = parsePlan(d?.plan);
      const finalMigration = plan?.length ? plan[plan.length - 1] : null;
      return {
        planVersion: d?.version,
        lastFileName: finalMigration?.fileName || null,
        lastSequence: finalMigration?.sequence || null,
      };
    });

    //read full version history from git logs (back to beginning of time)
    const { historicalPlans: fullHistoricalPlans } =
      await getHistoricalPlansFromGit(planFilePath);
    const fullGitVersionHistory = fullHistoricalPlans.map((p) => {
      const plan = parsePlan(p?.plan);
      const finalMigration = plan?.length ? plan[plan.length - 1] : null;
      return {
        gitCommitHash: p?.revision?.version,
        planVersion: p?.version,
        timeStamp: p?.revision?.timeStamp,
        lastFileName: finalMigration?.fileName || null,
        lastSequence: finalMigration?.sequence || null,
      };
    });
    const output = {
      fullDatabaseVersionHistory,
      fullGitVersionHistory,
    };
    console.log(JSON.stringify(output, null, 2));
  } catch (error) {
    console.error(chalk.red(error));
    console.error(chalk.red(historyError));
    throw new Error('Unable to print debug history');
  }
};
