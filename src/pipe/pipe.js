const errorMessage = (fileName, message, stack) =>
  `Unhandled exception in file ${fileName}: ${message}

  ${stack}
`;

/**
 * Executes the pending migrators on a payload, returns modified payload
 *
 * Loosely based on:
 *  - Eric Elliott's pipe implementation https://medium.com/javascript-scene/reduce-composing-software-fe22f0c39a1d
 *  - lodash flow https://lodash.com/docs/4.17.15#flow
 *
 * Implementation includes exception handling to extend errors with the filename
 * until module-with-string fixes its stack trace handling (see OW-8879 and
 * https://github.com/exuanbo/module-from-string/issues/18)
 *
 * @param {Array<Object>} migrations
 * @param {Object} payload
 * @returns {Object} migratedPayload
 */

export const pipe = (migrations, payload) =>
  migrations.reduce((v, m) => {
    if (typeof m?.migrator !== 'function') {
      throw new TypeError('Expected a function');
    }
    try {
      return m.migrator(v);
    } catch (error) {
      throw new Error(errorMessage(m?.fileName, error.message, error.stack));
    }
  }, payload);
