import { createHash } from 'crypto';

/**
 * Generate SHA256 hash from a string
 *
 * @param {String} string
 * @returns {String} SHA256 hash
 */

export const hash = (string) =>
  createHash('sha256').update(string).digest('hex');
