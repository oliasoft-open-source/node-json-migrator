import chalk from 'chalk';
import { getPlanFromVersion, insertVersions } from '../database/database';
import { validatePlan } from '../plan/validator';
import { getHistoricalPlansFromGit, historyError } from '../git/git';

/**
 * Get the executed migrations for a given payload version
 *
 * @param {Object} database database connection object
 * @param {Object} pgpHelpers pg-promise helpers
 * @param {String} entity entity name for payload
 * @param {String} directory path to migrations directory
 * @param {String} currentVersion
 * @param {String} plannedVersion
 * @param {Array<Object>} plannedMigrations planned migration entries
 * returns Promise<Array<Object>> executed migration entries
 */

export const getExecutedMigrationsFromVersion = async (
  database,
  pgpHelpers,
  entity,
  directory,
  currentVersion,
  plannedVersion,
  plannedMigrations,
) => {
  if (!currentVersion) {
    //when there is no current version, payload has never been migrated
    //so executed entries is empty
    return [];
  }
  if (currentVersion === plannedVersion) {
    //when planned version matches current version, there is nothing to migrate
    return plannedMigrations;
  } else {
    //otherwise we look up the historical plan file for that version from the DB
    let planFromVersion = await getPlanFromVersion(
      database,
      entity,
      currentVersion,
    );
    if (planFromVersion === null) {
      //if not in database, try to get it from the git history
      console.warn(
        chalk.yellow(
          `Version not found in ${entity}_version table, trying git history instead`,
        ),
      );
      const planFilePath = `${directory}/plan.json`;
      const { historicalPlans } = await getHistoricalPlansFromGit(planFilePath);
      //cache the response in the entity versions table
      if (historicalPlans.length) {
        await insertVersions(database, pgpHelpers, entity, historicalPlans);
        planFromVersion =
          historicalPlans.find((p) => p.version === currentVersion)?.plan ||
          null;
      }
    }
    if (planFromVersion !== null) {
      let migrationsFromHistory = null;
      try {
        migrationsFromHistory = JSON.parse(planFromVersion);
      } catch {
        throw new Error(
          `Invalid record in ${entity}_version table (cannot parse JSON)`,
        );
      }
      if (!validatePlan(migrationsFromHistory)) {
        throw new Error(
          `Invalid plan in ${entity}_version table (does not match schema)`,
        );
      } else {
        return migrationsFromHistory;
      }
    } else {
      console.error(chalk.red(historyError));
      throw new Error(`Unable to migrate, could not find version history`);
    }
  }
};
